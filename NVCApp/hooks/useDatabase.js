// force the state to clear with fast refresh in Expo
// @refresh reset
import React, {useEffect} from 'react';

import {database} from '../components/database'

export default function useDatabase() {
  const [isDBLoadingComplete, setDBLoadingComplete] = React.useState(false);

  useEffect(() => {
    async function loadDataAsync() {
      try {
        //wipe database
        //comment out for prod/////////////////
        //await database.dropDatabaseTablesAsync();
        console.log("Setting up database...");
        await database.setupDatabaseAsync();
        console.log("Database setup complete.");

        //await database.setupPeopleAsync();
        //await database.setupInteractionsAsync();

        console.log("Setting up settings...");
        await database.setupSettingsAsync();
        console.log("Settings setup complete.");


        setDBLoadingComplete(true);
        console.log("Database initialization complete. App is ready.");
      } catch (e) {
        console.warn(e);
      }
    }

    loadDataAsync();
  }, []);
//By including the [] as the second parameter, we only call this function on the initial render.
  return isDBLoadingComplete;
}