
import React, { useEffect, createContext, useState } from 'react';

import * as SQLite from "expo-sqlite"
const db = SQLite.openDatabase('db.db')
export const PeopleContext = createContext({});

export const PeopleContextProvider = props => {
  // Initial values are obtained from the props
  const {
    people: initialPeople,
    children
  } = props;

  // Use State to store the values
  const [people, setPeople] = useState(initialPeople || []);

  useEffect(() => {
    refreshPeople()
  }, [] )


  const addNewPerson = (personName) => {
  db.transaction( tx => {
      tx.executeSql( 'INSERT INTO people (name) values (?)', [personName] );
    },
    (t, error) => { console.log("db error insertPerson"); console.log(error);},
    (t, success) => { refreshPeople() }
  )
}

const updatePerson = (personName,personID) => {
  db.transaction( tx => {
      tx.executeSql( 'UPDATE people SET name= (?) WHERE id = (?)', [personName,personID] );
    },
    (t, error) => { console.log("db error insertPerson"); console.log(error);},
    (t, success) => { refreshPeople() }
  )
}
  const deletePerson = (personID) => {
  db.transaction( tx => {
      tx.executeSql( 'DELETE from interactions WHERE person_id = ?;', [personID] );
      tx.executeSql( 'DELETE from people WHERE id = ?;', [personID] );
    },
    (t, error) => { console.log("db error deletePerson"); console.log(error);},
    (t, success) => { refreshPeople() }
  )
}

const getPeople = (setPersonFunc) => {
  db.transaction(
    tx => {
      tx.executeSql(
        'select * from people',
        [],
        (_, { rows: { _array } }) => {
          setPersonFunc(_array)
        }
      );
    },
    (t, error) => { console.log("db error load people"); console.log(error) },
    (_t, _success) => { console.log("loaded people")}
  );
}
  const refreshPeople = () =>  {
    return getPeople(setPeople)
  }

  // Make the context object:
  const peopleContext = {
    people,
    addNewPerson,
    deletePerson,
    updatePerson
  };

  // pass the value in provider and return
  return <PeopleContext.Provider value={peopleContext}>{children}</PeopleContext.Provider>;
};