// force the state to clear with fast refresh in Expo
// @refresh reset

import React, { useEffect, createContext, useState } from 'react';

import * as SQLite from "expo-sqlite"
const db = SQLite.openDatabase('db.db')
export const InteractionsContext = createContext({});

export const InteractionsContextProvider = props => {
  // Initial values are obtained from the props
  const {
    interactions: initialInteractions,
    interactionId: initialInteractionsId,
    singleInteraction: initialSInteractions,
    dates: initialDate,
    children
  } = props;

  // Use State to store the values
  const [dates, setdates] = useState(initialDate);
  const [interactions, setInteractions] = useState(initialInteractions);
  const [interactionId, setInteractionId] = useState('');
   const [singleInteraction, setSingleInteraction] = useState('');
  
  useEffect(() => {
    refreshInteractions()
  }, [] )

 const insertInteraction = (interactionText, successFunc) => {
  db.transaction( tx => {
      tx.executeSql( 'INSERT INTO interactions (interaction) VALUES (?)', [interactionText] );
    },
    (t, error) => { console.log("db error insertInteraction"); console.log(error);},
    (t, success) => { refreshInteractions() }
  )
}

const deleteInteraction = (interactionID) => {
  db.transaction( tx => {
      tx.executeSql( 'delete from interactions where id = ?;', [interactionID] );
    },
    (t, error) => { console.log("db error deleteInteraction"); console.log(error);},
    (t, success) => { console.log("delete interaction") }
  )
}

const getInteractions = (setInteractionFunc) => {
  db.transaction(
    tx => {
      tx.executeSql(
        'SELECT * FROM interactions',
        [],
        (_, { rows: { _array } }) => {
          setInteractionFunc(_array)
        }
      );
    },
    (t, error) => { console.log("db error load interactions"); console.log(error) },
    (_t, _success) => { console.log("loaded interactions")}
  );
}
  const refreshInteractions = () =>  {
    return getInteractions(setInteractions)
  };


const addNewInteraction = (date,type,resolved, personID) => {
  db.transaction( tx => {
      tx.executeSql( `INSERT INTO interactions (occurrence_date,interaction,resolved,person_id) VALUES (?,?,?,?);`, 
      [date,type,resolved, personID]
       );

    },
    (t, error) => { console.log("db error addNewInteraction"); console.log(error);},
    (t, success) => { getInteractionsId()  }
  )
}

const addNewInteractionObservation = (interactionObservationText, iid) => {
  db.transaction( tx => {
      tx.executeSql( `UPDATE interactions SET observation= (?) WHERE id = (?)`, 
      [interactionObservationText,iid]
       );

    },
    (t, error) => { console.log("db error insertInteractionObservation"); console.log(error);},
    (t, success) => { console.log("insert observation"); }
  )
}

const addNewInteractionEmotion = (interactionEmotionText,iid) => {
  db.transaction( tx => {

      tx.executeSql( 'UPDATE interactions SET emotion= (?) WHERE id = (?)', [interactionEmotionText,iid] );
    },
    (t, error) => { console.log("db error insertInteractionEmotion"); console.log(error);},
    (t, success) => {  }
  )
}

const addNewInteractionNeed = (interactionNeedText,iid) => {
  db.transaction( tx => {
      tx.executeSql( 'UPDATE interactions SET need= (?) WHERE id = (?)', [interactionNeedText,iid] );
    },
    (t, error) => { console.log("db error insertInteractionNeed"); console.log(error);},
    (t, success) => { }
  )
}

  const addNewInteractionRequest = (interactionRequestText,iid) => {
    db.transaction( tx => {
        tx.executeSql( 'UPDATE interactions SET request= (?) WHERE id = (?)', [interactionRequestText,iid] );
      },
      (t, error) => { console.log("db error insertInteractionRequest"); console.log(error);},
      (t, success) => {  }
    )
  }

const getInteractionsId = () => {
  db.transaction(
    (tx) => {
      tx.executeSql(
        'SELECT last_insert_rowid();', 
        [],
        (_, { rows: { _array } }) => {
          setInteractionId(_array[0]["last_insert_rowid()"])
        }
      );
    },
    (t, error) => { console.log("db error interaction ID!"); console.log(error) },
    (_t, _success) => { console.log("interaction id gotten!")}
  );
  }


  const setInteractionResolution = (interactionResolution, id) => {
    db.transaction( tx => {
        tx.executeSql( 'UPDATE interactions SET resolved= (?) WHERE id = (?)', [interactionResolution,id] );
      },
      (t, error) => { console.log("db error setInteractionResolution"); console.log(error);},
      (t, success) => { console.log("setInteractionResolution")}
    )
  }

  const getRelevantInteractions= (personID) => {
  db.transaction(
    (tx) => {
      tx.executeSql(
        'SELECT * FROM interactions WHERE person_id = ? ORDER BY occurrence_date DESC', 
        [personID],
        (_, { rows: { _array } }) => {
          setInteractions(_array)
        }
      );
    },
    (t, error) => { console.log("db error load relevant interactions"); console.log(error) },
    (_t, _success) => { console.log("loaded relevant interactions")}
  );
}
const getRelevantInteractionsByDate= (personID,date) => {
  db.transaction(
    (tx) => {
      tx.executeSql(
        'SELECT * FROM interactions WHERE person_id = ? AND occurrence_date = ?', 
        [personID,date],
        (_, { rows: { _array } }) => {
          setInteractions(_array)
        }
      );
    },
    (t, error) => { console.log("db error load relevant interactions"); console.log(error) },
    (_t, _success) => { console.log("loaded relevant interactions by date")}
  );
}
const getAllRelevantInteractionsByDate= (date) => {
  db.transaction(
    (tx) => {
      tx.executeSql(
        'SELECT interactions.id, occurrence_date,observation,interaction,emotion,need, request, resolved,person_id, name FROM interactions JOIN people ON people.id = interactions.person_id WHERE occurrence_date = ?', 
        [date],
        (_, { rows: { _array } }) => {
          setInteractions(_array)
        }
      );
    },
    (t, error) => { console.log("db error load relevant interactions"); console.log(error) },
    (_t, _success) => { console.log("loaded ALL relevant interactions by date")}
  );
}
const getRelevantDatesbyPersonID= (personID) => {
  db.transaction(
    (tx) => {
      tx.executeSql(
        'SELECT occurrence_date FROM interactions WHERE person_id = ? GROUP BY occurrence_date', 
        [personID],
        (_, { rows: { _array } }) => {
          setdates(_array)
        }
      );
    },
    (t, error) => { console.log("db error load relevant interactions"); console.log(error) },
    (_t, _success) => { console.log("loaded relevant dates by personid")}
  );
}

const getAllInteractionDates= () => {
  db.transaction(
    (tx) => {
      tx.executeSql(
        'SELECT occurrence_date FROM interactions GROUP BY occurrence_date', 
        [],
        (_, { rows: { _array } }) => {
          setdates(_array)
        }
      );
    },
    (t, error) => { console.log("db error load relevant interactions"); console.log(error) },
    (_t, _success) => { console.log("loaded relevant dates by personid")}
  );
}

const getSingleInteraction = (interactionId) => {
  db.transaction(
    tx => {
      tx.executeSql(
        'SELECT * FROM interactions WHERE id=?', 
        [interactionId],
        (_, { rows: { _array } }) => {
          setSingleInteraction(_array[0])
        }
      );
    },
    (t, error) => { console.log("db error load interactions"); console.log(error) },
    (_t, _success) => { console.log("loaded single interaction")}
  );
}

  // Make the context object:
  const interactionsContext = {
    interactions,
    interactionId,
    insertInteraction,
    deleteInteraction,
    setInteractionId,
    getRelevantInteractions,
    getRelevantInteractionsByDate,
    getAllRelevantInteractionsByDate,
    getAllInteractionDates,
    getRelevantDatesbyPersonID,
    addNewInteraction,
    addNewInteractionObservation,
    addNewInteractionEmotion,
    addNewInteractionNeed,
    addNewInteractionRequest,
    setSingleInteraction,
    setInteractionResolution,
    getSingleInteraction,
    getInteractionsId,
    singleInteraction,
    dates 
  };

  // pass the value in provider and return
  return <InteractionsContext.Provider value={interactionsContext}>{children}</InteractionsContext.Provider>;
};