
import React, {  createContext,useState } from 'react';

import * as SQLite from "expo-sqlite"
const db = SQLite.openDatabase('db.db')
export const SettingsContext = createContext({});

export const SettingsContextProvider = props => {
   // Initial values are obtained from the props
   const {
    walkthroughValue:initialWalkthroughValue,
    children
  } = props;
  // Use State to store the values
  const [walkthroughValue, setWalkthroughValue] = useState('');

const updateSetting = (mySettingName,myvalue) => {
  db.transaction( tx => {
      tx.executeSql( 'UPDATE settings SET value= (?) WHERE name = (?)', [myvalue,mySettingName] );
    },
    (t, error) => { console.log("db error updateSetting"); console.log(error);},
    (t, success) => { console.log("setting updated") }
  )
}

const getSetting = (mySettingName) => {
  db.transaction(
    tx => {
      tx.executeSql(
        'SELECT value FROM settings WHERE name = (?)',[mySettingName],
        (_, { rows: { _array } }) => {
          setWalkthroughValue(_array[0]['value']=='on')
        }
      );
    },
    (t, error) => { console.log("db error get setting"); console.log(error) },
    (_t, _success) => { console.log("my walkthrough val " + walkthroughValue )}
  );
}
  
  // Make the context object:
  const settingsContext = {
    getSetting,
    updateSetting,
    walkthroughValue
  };

  // pass the value in provider and return
  return <SettingsContext.Provider value={settingsContext}>{children}</SettingsContext.Provider>;
};