import 'react-native-gesture-handler';
import * as React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import {useContext} from 'react';
import { GestureHandlerRootView } from 'react-native-gesture-handler';

import { NavigationContainer ,useNavigation, useRoute, goBack} from '@react-navigation/native';
import {  Text,View }from 'react-native';
//navigation
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {OverflowMenuProvider} from 'react-navigation-header-buttons';
import {
  HeaderButtons,
  HiddenItem,
  OverflowMenu,
  Divider,
} from 'react-navigation-header-buttons';
import MaterialHeaderButton from './components/MaterialHeaderButton';
import {DrawerActions} from '@react-navigation/native';
//Components and Contexts
import useDatabase from './hooks/useDatabase.js'
import {PeopleContextProvider} from './context/PeopleContext';
import {InteractionsContextProvider,InteractionsContext} from './context/InteractionsContext';
import {SettingsContextProvider} from './context/SettingsContext';
import * as SplashScreen from 'expo-splash-screen';
import useCachedResources from './hooks/useCachedResources';

//Screens
import IntroScreen from './screens/IntroScreen';
import PrivacyScreen from './screens/PrivacyScreen';
import HistoryScreen from './screens/HistoryScreen';
import AboutScreen from './screens/AboutScreen';
//import HomeScreen from './screens/HomeScreen';
import HomeCalendarScreen from './screens/HomeCalendarScreen';
import ChoosePersonScreen from './screens/ChoosePersonScreen';
import PersonsScreen from './screens/PersonsScreen';
import InteractionsScreen from './screens/InteractionsScreen';
import InteractionCalendarScreen from './screens/InteractionCalendarScreen';
import NewInteractionScreen from './screens/NewInteractionScreen';
import TypeHelpScreen from './screens/TypeHelpScreen';
import ObservationScreen from './screens/ObservationScreen';
import ObservationHelpScreen from './screens/ObservationHelpScreen';
import EmotionQuadrantScreen from './screens/EmotionQuadrantScreen';
import EmotionScreen2 from './screens/EmotionScreen2';
import EmotionEnergyScreen from './screens/EmotionEnergyScreen';
import EmotionPleasantnessScreen from './screens/EmotionPleasantnessScreen';
import EmotionUnpleasantnessScreen from './screens/EmotionUnpleasantnessScreen';
import NeedScreen from './screens/NeedScreen';
import NeedScreen2 from './screens/NeedScreen2';
import NeedsHelpScreen from './screens/NeedsHelpScreen';
import RequestHelpScreen from './screens/RequestHelpScreen';
import WriteRequestScreen from './screens/WriteRequestScreen';
import SummaryGratitude from './screens/SummaryGratitude';
import SummaryScreen from './screens/SummaryScreen';
import SummaryHelpScreen from './screens/SummaryHelpScreen';
import HelpScreen from './screens/HelpScreen';
import WalkthroughScreen from './screens/WalkthroughScreen';
//styles
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import {fontPixel} from './components/styleNormalization';
import { StatusBar } from 'expo-status-bar';


function MyDrawer({route,navigation}) {
  const dateParam = route.params.dateParam;

  return (
    <Drawer.Navigator 

    screenOptions={{
      headerLeft: () => {
        //top right drawer icon
        return <AntDesign name="menu-fold" size={24} style={{paddingLeft:10}} color="#222" onPress={() => {navigation.dispatch(DrawerActions.openDrawer());}}/>
      },  
      drawerActiveTintColor: '#888',
     }}
     >
      <Stack.Screen name="Home" component={HomeCalendarScreen} initialParams={{dateParam:dateParam}} 
       options={{ 
        headerTitle: 'Home' ,
        headerTitleStyle: {
                color: "#31989e",
                fontSize:fontPixel(23),
              },
        drawerIcon:() => <Ionicons name="home" size={24} color="#888" />
      }} 
         />
      <Drawer.Screen name="Add/Edit People" component={PersonsScreen} 
      options={() => { return{ headerTransparent: false, headerTitle:"Add/Edit People", drawerIcon:() => <AntDesign name="adduser" size={24} color="#888" />}}}/>
      <Drawer.Screen name="Help" component={HelpScreen} 
      options={() => { return{ headerTransparent: true, headerTitle:"", drawerIcon:() => <Ionicons name="person-add-outline" size={24} color="#888" />}}}/>
      
      <Drawer.Screen name="About this App" component={AboutScreen} options={() => { return{ headerTransparent: true, headerTitle:"" }}}/>
      <Drawer.Screen name="History" component={HistoryScreen}  options={() => { return{ headerTransparent: true, headerTitle:"" }}} />
      <Drawer.Screen name="Privacy" component={PrivacyScreen}  options={() => { return{ headerTransparent: true, headerTitle:"" }}} />
    </Drawer.Navigator>
  );
}


function MyMenu(){
  const navigation = useNavigation();
  const interactionsContext = useContext(InteractionsContext);
  const { singleInteraction} = interactionsContext;
  return(
    <HeaderButtons HeaderButtonComponent={MaterialHeaderButton}>
    <OverflowMenu
      style={{ marginHorizontal: 10 }}
      OverflowIcon={({ color }) => (
        <MaterialIcons name="more-horiz" size={23} color="grey" />
      )}
    >
      <HiddenItem title="Home"  onPress={() => {
        navigation.navigate('MyDrawer', {
    screen: 'Home',
    params: { dateParam:singleInteraction.occurrence_date },
  });
      }} />
    
    </OverflowMenu>
  </HeaderButtons>
  )
}
//<Stack.Screen name="Help" component={HelpScreen}  options={() => { return{ headerTransparent: true, headerTitle:"" }}}  />
const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();



export default function App() {
  SplashScreen.preventAutoHideAsync();//don't let the splash screen hide 
  const isLoadingComplete = useCachedResources();
  const isDBLoadingComplete = useDatabase();
  //debugging
  console.log('Resource loading complete:', isLoadingComplete);
  console.log('Database loading complete:', isDBLoadingComplete);
  

  if ( isDBLoadingComplete && isLoadingComplete) {
    
    SplashScreen.hideAsync();
      return (
        <GestureHandlerRootView style={{ flex: 1 }}>
        <NavigationContainer>
         <SettingsContextProvider>
         <PeopleContextProvider>
         <InteractionsContextProvider>
         <StatusBar style="light" />
         <OverflowMenuProvider>
          <Stack.Navigator initialRouteName="Home"     
          screenOptions={{
            headerRight: () => ( <MyMenu />),
            headerShadowVisible:false,
            headerBackTitleVisible: false,
            headerTintColor: '#888',
            headerTitleStyle: {
                color: "#31989e",
                fontSize:fontPixel(23),
              },
           }}>
            <Stack.Screen name="Intro Screen" component={IntroScreen} options={{headerShown: false}} />
            <Stack.Screen
              name="MyDrawer"
              component={MyDrawer}
              options={{ headerShown: false }}
            />
            <Stack.Screen name="People" component={PersonsScreen} 
              options={{
                headerTitle: "Add/Edit Person",
            }}
            />
            <Stack.Screen name="WalkthroughScreen" component={WalkthroughScreen}  options={{headerShown: false}} />
            <Stack.Screen name="HomeCalendarScreen" component={HomeCalendarScreen}  />
            <Stack.Screen name="Person Involved" component={ChoosePersonScreen} options={{ title: "Select Person Involved" }}  />
            <Stack.Screen name="Interactions" component={InteractionsScreen}  />
            <Stack.Screen name="Calendar" component={InteractionCalendarScreen} options={{ title: "Interactions Calendar" }}  />
            <Stack.Screen name="New Interaction" component={NewInteractionScreen} options={{ title: "Type" }}  />
            <Stack.Screen name="Type Help" component={TypeHelpScreen} options={{title:"Conflict or Gratitude?"}} />   
            <Stack.Screen name="Observation" component={ObservationScreen} options={() => { return{  headerTitle:"" }}}  />
            <Stack.Screen name="Observation Help" component={ObservationHelpScreen}  /> 
            <Stack.Screen name="Energy" component={EmotionEnergyScreen}  options={({ navigation }) => {
                return {
                  headerTransparent: true, headerTitle:"",headerTitleStyle: {color: "rgba(237,184,43,1)"},
                  headerLeft: () => 
                  <Ionicons name="chevron-back-outline" size={32} 
                    onPress={() => navigation.navigate('Observation' )} color="#888" />}}}
                  />
              <Stack.Screen name="Pleasantness" component={EmotionPleasantnessScreen} options={() => { return{  headerTransparent: true, headerTitle:""  }}}  />
              <Stack.Screen name="Unpleasantness" component={EmotionUnpleasantnessScreen} options={() => { return{  headerTransparent: true, headerTitle:""  }}}  />
              <Stack.Screen name="Emotion List" component={EmotionScreen2} options={() => { return{  headerTransparent: true, headerTitle:""  }}}  />
              <Stack.Screen name="Emotion Help" component={EmotionQuadrantScreen} options={() => { return{   headerTitle:"Emotion Help"  }}}  />

            <Stack.Screen name="Need" component={NeedScreen}  options={({ navigation }) => {
                return {
                   headerTitle:"",headerTitleStyle: {color: "#fff"},
                  headerLeft: () => 
                  <Ionicons name="chevron-back-outline" size={32} 
                    onPress={() => navigation.navigate('Energy' )} color="#888" />}}}
                  />
            <Stack.Screen name="Need List" component={NeedScreen2} options={() => { return{  headerTitle:""  }}} />            
            <Stack.Screen name="Needs Help" component={NeedsHelpScreen} options={{title:"What Are Needs?"}}  />   
            <Stack.Screen name="Request Help" component={RequestHelpScreen}  />        
            <Stack.Screen name="Write Request" component={WriteRequestScreen} options={({ navigation }) => {
                return {
                   headerTitle:"",headerTitleStyle: {color: "#fff"},
                  headerLeft: () => 
                  <Ionicons name="chevron-back-outline" size={32} 
                    onPress={() => navigation.navigate('Need' )} color="#888" />}}}
                  />
            <Stack.Screen name="Summary of Gratitude" component={SummaryGratitude} options={({ navigation }) => {
                return {
                   headerTitle:"",headerTitleStyle: {color: "#fff"},
                  headerLeft: () => 
                  <Ionicons name="chevron-back-outline" size={32} 
                    onPress={() => navigation.navigate('Need' )} color="#888" />}}}
                  />
            <Stack.Screen name="Summary" component={SummaryScreen} options={({ navigation }) => {
                return {
                   headerTitle:"",headerTitleStyle: {color: "#fff"},
                  headerLeft: () => 
                  <Ionicons name="chevron-back-outline" size={32} 
                    onPress={() => navigation.navigate('Write Request' )} color="#888" />}}}
                  />
            <Stack.Screen name="Summary Help" component={SummaryHelpScreen}  />

          </Stack.Navigator>
          </OverflowMenuProvider>
          </InteractionsContextProvider>
          </PeopleContextProvider>
          </SettingsContextProvider>
        </NavigationContainer>
        </GestureHandlerRootView>
       
      );  
      } else {
        return null;
      }
}

