const needsDict = {
	autonomy: {
		name: "Autonomy",
		color: "#ff0000",
		children: [
			{
				name:"autonomy",
				children:[
					{ name: "Go for a drive by yourself" },
					{ name: "Do something for yourself that you have been putting off" },
                ]
			},
		    {				
				name: "choice",
				children: [null]
			},
			{
				name: "creativity",
				children: [
					{ name: "Do something artistic" },
					{ name: "Try a new craft" },
					{ name: "Try a new hobby" },
                ]
			},
			{
				name:"empowerment",
				children:[
					{ name: "Put yourself out of your comfort zone" },
            ]
			},
			{
				name: "freedom",
				children: [null]
			},
			{
				name:"personal space",
				children:[
					{ name: "Spend 10 minutes in a quiet place" },
					{ name: "Kindly ask for space" },
                ]
			},
			{
				name:"wholeness",
				children:[
					{ name: "Take time to reconnect with yourself" },
            ]
			},
        ]
	},
	play: {
		name: "Play or Relaxation",
		color: "#ff0000",
		children: [

			{				
				name: "celebration",
				children: [
					{ name: "Spend time with a loved one" },
					{ name: "Treat Yourself" },
				]
			},
			{				
				name: "fun",
				children: [null]
			},
            {				
				name: "humor",
				children: [
					{ name: "Watch a funny video" },
					{ name: "Talk to a friend that makes you laugh" },
                ]
			},
            {				
				name: "leisure",
				children: [
					{ name: "Watch something" },
					{ name: "Lay down under the sun" },
                ]
			},
			{
				name: "play",
				children: [
					{ name: "Play a game" },
					{ name: "Go out in nature" },
					{ name: "Play a sport/activity" },
					{ name: "Go swimming" },
                ]
			},
			{				
				name: "relaxation",
				children: [
					{ name: "Meditate" },
					{ name: "Go out in nature" },
					{ name: "Take a bath" },
					{ name: "Listen to soothing music" },
					{ name: "Take deep breaths" },
                ]
			},
            {				
				name: "stimulation",
				children: [
					{ name: "Squeeze a squishy ball" },
					{ name: "Exercise/get your body moving" },
                    { name: "Take a course at a school and/or online" },
                    { name: "Make something" },
                ]
			},
        ]
	},
	connection: {
		name: "Connection with others",
		color: "#ff0000",
		children: [
			{
				name: "acceptance",
				children: null
			},
            {				
				name: "care",
				children: null
			},
            {				
				name: "community",
				children: [
                    { name: "Find a community service project" },
					{ name: "Engage in community events" },
					{ name: "Say hello to someone in your neighborhood" },
                ]
			},
			{				
				name: "communication",
				children: [
                    { name: "Have a chat with a friend/family member" },
                ]
			},
			{				
				name: "connection",
				children: [
                ]
			},
			{				
				name: "consideration",
				children: [
                ]
			},
			{				
				name: "empathy",
				children: [
                    { name: "Speak to a mental health professional" },
					{ name: "Speak to an understanding friend/family member" },
                ]
			},
			{				
				name: "feedback",
				children: [
                    { name: "Speak to a trusted friend/family member" },
					{ name: "Speak to a teacher/mentor" },
					{ name: "Speak to a coach" },
                ]
			},

			{				
				name: "intimacy",
				children: [
                ]
			},
			{				
				name: "mourning",
				children: [
					{ name: "Speak to a loved one" },
					{ name: "Let yourself express sadness" },
                ]
			},
			{				
				name: "recognition",
				children: [
                ]
			},
			{				
				name: "respect",
				children: [
                ]
			},
	

        ]
	},
	physicalwellbeing: {
		name: "Physical Well-Being",
		color: "#ff0000",
		children: [
			{
				name: "air",
				children: [
					{ name: "Go outside" },
                ]
			},
			{
				name: "food",
				children: [
					{ name: "Eat a snack" },
                ]
			},

			{
				name: "health/healing",
				children: [
					{ name: "Visit a doctor" },
					{ name: "Set health goals" },
                ]
			},
			{
				name: "movement",
				children: [
					{ name: "Go outside" },
					{ name: "Exercise" },
					{ name: "Dance" },
					{ name: "Play a sport" },
                ]
			},
			{
				name: "rest/sleep",
				children: [
					{ name: "Take a nap" },
					{ name: "Lie down" },
                ]
			},
			{
				name: "safety",
				children: [

                ]
			},
			{
				name: "sexual expression",
				children: [

                ]
			},
			{
				name: "shelter",
				children: [

                ]
			},
			{
				name: "touch",
				children: [
					{ name: "Hug a loved one" },
                ]
			},
			{
				name: "water",
				children: [
					{ name: "Drink a glass of water" },
                ]
			},


        ]
	},
    psychologicalwellbeing: {
		name: "Psychological Well-Being",
		color: "#ff0000",
		children: [
			{				
				name: "care",
				children: null
			},

			{
				name: "competence",
				children: [
					{ name: "Complete something on your to-do list" },
                ]
			},

			{
				name: "consistency",
				children: [
					{ name: "Create a schedule for yourself that is not reliant on others" },
                ]
			},
			{
				name: "discovery",
				children: [
					{ name: "Discover a new activity" },
					{ name: "Try something new" },
					{ name: "Go to a place you've never been before" },
                ]
			},
			{
				name: "growth",
				children: null
			},
			{
				name: "learning",
				children: [
					{ name: "Learn a new skill" },
                ]
			},
			{
				name: "presence",
				children: [
					{ name: "Mindfulness" },
					{ name: "Do a body scan" },

                ]
			},
			{
				name: "self-reliance",
				children: [
					{ name: "Build something for yourself" },
					{ name: "Do something for yourself" },
                ]
			},

			{
				name: "safety",
				children: [

                ]
			},
			{
				name: "self-connection",
				children: [
					{ name: "Meditation/mindfulness" },
                ]
			},
			{
				name: "self-compassion",
				children: [
					{ name: "Think of 3 things you love about yourself" },
                ]
			},
			{
				name: "self-expression",
				children: [
					{ name: "Do something artistic" },
					{ name: "Play or make music" },
					{ name: "Write in a journal" },
                ]
			},

			{
				name: "spontaneity",
				children: [
					{ name: "Try something you've never tried before" },
                ]
			},


        ]
	},
}

export {needsDict}