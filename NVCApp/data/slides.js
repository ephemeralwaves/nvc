import styles from '../components/stylesComponent';
const slides = [
    {
      key: '0',
      title: 'Let\'s get started',
      toptext: 'At its core, Nonviolent Communication is about communicating honestly and receiving empathetically, a way of communicating that “leads us to give from the heart” (Rosenberg). For conflicts, this app will walk you through the four key parts: ',
      bottomtext:' These steps are used to create statements that you can use with the person you are in conflict with or share as a gratitude. It can also be used as a way to self-reflect or listen more empathetically.', 
      image: {
        require:
        require("../images/ccycle.png"),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      imageStyle: styles.walkthroughImageCenter,
      backgroundColor: '#DD7673',
    },
    {
      key: '1',
      title: 'Creating a New Interaction',
      toptext: 'Select the day the interaction happened and press "New Interaction". Press Add Person and enter the name. Then tap on their name and select the interaction type.',
      video: {
        uri:
        require('../assets/walkthrough/walkthrough1.mp4'),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      videoStyle: styles.walkthroughVideoCenter,
      backgroundColor: '#DD7673',
    },
    {
      key: '2',
      title: 'Observation',
      toptext: 'Describe the interaction as neutrally as possible. These statements lay out bare facts without judgements or assumptions. You can press the Help menu to see some examples. ',
      video: {
        uri:
        require('../assets/walkthrough/observation.mp4'),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      videoStyle: styles.walkthroughVideoCenter,
      backgroundColor: '#DD7673',
    },
    {
      key: '3',
      title: 'Emotion',
      toptext: `
      After entering your observation, you will be asked questions about how you are feeling. 

      The help menu shows a graph that displays the emotional states a personal can experience and the associated emotions.`,
      image: {
        require:
        require("../assets/walkthrough/quadrant.gif"),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      imageStyle: styles.walkthroughImageGifCenter,
      backgroundColor: '#DD7673',
    },
    {
      key: '4',
      title: 'Choosing the Emotion',
      toptext: `You are first asked about your energy level. Then about your amount of pleasantness (for gratitudes) or unpleasantness (for conflict). Lastly, choose from the list of associated emotions. You can go back at any time.`,
      video: {
        uri:
        require('../assets/walkthrough/emotion.mp4'),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      videoStyle: styles.walkthroughVideoCenter,
      backgroundColor: '#DD7673',
    },
    {
      key: '5',
      title: 'Need',
      toptext: `This app attempts to explain emotions due to met or unmet universal needs. You will select the need that you think is at the root of your emotion. You can optionally select a more specific need or skip it. 
      `,
      video: {
        uri:
        require('../assets/walkthrough/need.mp4'),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      videoStyle: styles.walkthroughVideoCenter,
      backgroundColor: '#DD7673',
    },
    {
      key: '6',
      title: 'Request',
      toptext: 'Write a request that can satisfy the need. Requests, like observations, are non-judgmental and are not demands. They may be refused, in which case you can start the cycle again to find other strategies to fulfilling your need.',
      bottomtext:'Requests should be positive(use "for" language instead of "against" language), attainable in the short term, and clear. The help menu provides examples.', 
      image: {
        require:
        require("../assets/walkthrough/request.jpg"),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      imageStyle: styles.walkthroughImageCenter,
      backgroundColor: '#DD7673',
    },
    {
      key: '7',
      title: 'Summary',
      toptext: `Here is the sum of your efforts. If you press and hold on the text, you will be able to select and copy it in order to share it. You can mark if the request was accepted or not. 
      `,
      video: {
        uri:
        require('../assets/walkthrough/summary.mp4'),
      },
      titleStyle: styles.walkthroughWhiteCenterHeaderText,
      textStyle: styles.walkthroughWhiteCenterText,
      videoStyle: styles.walkthroughVideoCenter,
      backgroundColor: '#DD7673',
    },
    {
        key: '8',
        title: 'Edit, Save, Delete',
        toptext: `Information is auto-saved after each step and can be changed at any time. If you close the app or go Home through the Help menu, you can tap on that interaction and resume it. Long press on an interaction to go to the Summary where it can be deleted.
        `,
        video: {
          uri:
          require('../assets/walkthrough/editsavedelete.mp4'),
        },
        titleStyle: styles.walkthroughWhiteCenterHeaderText,
        textStyle: styles.walkthroughWhiteCenterText,
        videoStyle: styles.walkthroughVideoCenter,
        backgroundColor: '#DD7673',
      },
  ];

  export {slides}