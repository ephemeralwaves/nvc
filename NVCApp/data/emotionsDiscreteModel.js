const emotionDict = {
	angry: {
		name: "angry",
		color: "#C90E5A",
		children: [
			{
				name: "angry",
				children: [

				]
			},
			 {
				name: "yearning",
				children: [
					{ name: "envious" },
					{ name: "jealous" },
					{ name: "longing" },
					{ name: "nostalgic" },
					{ name: "wistful" },
				]
			},
			 {				
				name: "annoyed",
				children: [
					{ name: "aggravated" },
					{ name: "dismayed" },
					{ name: "disgruntled" },
					{ name: "displeased" },
					{ name: "exasperated" },
					{ name: "frustrated" },
					{ name: "impatient" },
					{ name: "irritated" },
					{ name: "irked" },
					{ name: "cranky" },
					{ name: "irritable" },
				]
			},
			 {
				name: "furious",
				children: [
					{ name: "enraged" },
					{ name: "aggressive" },
					{ name: "incensed" },
					{ name: "indignany" },
					{ name: "irate" },
					{ name: "livid" },
				]
			},
			 {
				name:"fearful",
				children:[
					{ name: "afraid" },
					{ name: "apprehensive" },
					{ name: "dread" },
					{ name: "foreboding" },
					{ name: "frightened" },
					{ name: "mistrustful" },
					{ name: "panicked" },
					{ name: "scared" },
					{ name: "suspicious" },
					{ name: "terrified" },
					{ name: "wary" },
					{ name: "worried" },
				]
			},
			{
				name:"vulnerable",
				children:[
					{ name: "fragile" },
					{ name: "guarded" },
					{ name: "helpless" },
					{ name: "insecure" },
					{ name: "leery" },
					{ name: "sensitive" },
					{ name: "shaky" },
				]
			},
			 {
				name:"embarrassed",
				children:[
					{ name: "ashamed" },
					{ name: "chagrined" },
					{ name: "flustered" },
					{ name: "guilty" },
					{ name: "mortified" },
					{ name: "self-conscious" },
				]

			},
			{
				name:"anxious",
				children:[
					{ name: "tense" },
					{ name: "distressed" },
					{ name: "distraught" },
					{ name: "edgy" },
					{ name: "fidgety" },
					{ name: "frazzled" },
					{ name: "flustered" },
					{ name: "jittery" },
					{ name: "nervous" },
					{ name: "overwhelmed" },
					{ name: "restless" },
					{ name: "stressed out" },
				]

			},
			 {
				name:"resentful",
				children:[
					{ name: "aggreived" },
					{ name: "displeased" },
					{ name: "discontented" },
					{ name: "dissatisfied" },
					{ name: "irritated" },

				]
			},
			 {
				name:"aversion",
				children:[
					{ name: "animosity" },
					{ name: "appalled" },
					{ name: "contempt" },
					{ name: "dislike" },
					{ name: "hate" },
					{ name: "horrified" },
					{ name: "hostile" },
					{ name: "repulsed" },
				]
			},
			{
				name:"uneasy",
				children:[
					{ name: "disturbed" },
					{ name: "perturbed" },
					{ name: "rattled" },
					{ name: "troubled" },
					{ name: "turbulent" },
					{ name: "turmoil" },
					{ name: "uncomfortable" },
					{ name: "upset" },
					{ name: "unsettled" },
				]
			},
			 {
				name:"confused",
				children:[
					{ name: "ambivalent" },
					{ name: "baffled" },
					{ name: "rabewilderedttled" },
					{ name: "dazed" },
					{ name: "hesitant" },
					{ name: "lost" },
					{ name: "mystified" },
					{ name: "perplexed" },
					{ name: "puzzled" },
					{ name: "torn" },
				]
			},
			{
				name:"startled",
				children:[
					{ name: "shocked" },
					{ name: "alarmed" },
				]
			},
		]
	},
	happy: {
		name: "happy",
		color: "#EDB92B",
		children: 
		[
			{
				name: "happy",
				children: [

				]
			},
				{
				name: "joyful",
				children: [
					{ name: "amused" },
					{ name: "delighted" },
					{ name: "glad" },
					{ name: "jubilant" },
					{ name: "pleased" },
					{ name: "tickled" },
				]
			},
			 {				
				name: "engaged",
				children: [
					{ name: "absorbed" },
					{ name: "alert" },
					{ name: "curious" },
					{ name: "engrossed" },
					{ name: "enchanted" },
					{ name: "entranced" },
					{ name: "fascinated" },
					{ name: "interested" },
					{ name: "intrigued" },
					{ name: "spellbound" },
					{ name: "stimulated" },
					{ name: "inspired" },
					{ name: "amazed" },
					{ name: "awed" },
					{ name: "wonder" },
				]
			},
			{				
				name: "excited",
				children: [
					{ name: "amazed" },
					{ name: "animated" },
					{ name: "ardent" },
					{ name: "aroused" },
					{ name: "astonished" },
					{ name: "dazzled" },
					{ name: "eager" },
					{ name: "energetic" },
					{ name: "enthusiastic" },
					{ name: "giddy" },
					{ name: "invigorated" },
					{ name: "lively" },
					{ name: "passionate" },
					{ name: "surprised" },
					{ name: "vibrant" },
				]
			},
			 {				
				name: "exhilarated",
				children: [
					{ name: "blissful" },
					{ name: "ecstatic" },
					{ name: "elated" },
					{ name: "enthralled" },
					{ name: "exuberant" },
					{ name: "radiant" },
					{ name: "rapturous" },
					{ name: "thrilled" },
			 	]
			},
		]
	},
	calm: {
		name: "calm",
		color: "#209754",
		children: [
			{
				name: "calm",
				children: [

				]
			},
			 {
				name: "peaceful",
				children: [
					{ name: "clear headed" },
					{ name: "comfortable" },
					{ name: "centered" },
					{ name: "content" },
					{ name: "equanimous" },
					{ name: "mellow" },
					{ name: "quiet" },
					{ name: "relaxed" },
					{ name: "rested" },
					{ name: "relieved" },
					{ name: "satisfied" },
					{ name: "serene" },
					{ name: "still" },
					{ name: "tranquil" },
					{ name: "trusting" },
				]
			},
			{				
				name: "confident",
				children: [
					{ name: "empowered" },
					{ name: "open" },
					{ name: "proud" },
					{ name: "safe" },
					{ name: "secure" },
					{ name: "hopeful" },
					{ name: "expectant" },
					{ name: "encouraged" },
					{ name: "optimistic" },
				]
			},
			 {				
				name: "grateful",
				children: [
					{ name: "appreciative" },
					{ name: "moved" },
					{ name: "thankful" },
					{ name: "touched" },
				]
			},
		]
	},
	sad: {
		name: "sad",
		color: "#2647E0",
		children: [
			{
				name: "sad",
				children: [

				]
			},
			 {
				name: "pain",
				children: [
					{ name: "agony" },
					{ name: "anguished" },
					{ name: "bereaved" },
					{ name: "devastated" },
					{ name: "grief" },
					{ name: "heartbroken" },
					{ name: "hurt" },
					{ name: "lonely" },
					{ name: "miserable" },
					{ name: "regretful" },
					{ name: "remorseful" },
				]
			},
			 {				
				name: "disconnected",
				children: [
					{ name: "alienated" },
					{ name: "aloof" },
					{ name: "apathetic" },
					{ name: "bored" },
					{ name: "cold" },
					{ name: "detached" },
					{ name: "distant" },
					{ name: "distracted" },
					{ name: "indifferent" },
					{ name: "numb" },
					{ name: "removed" },
					{ name: "uninterested" },
					{ name: "withdrawn" },
				]
			},
			 {				
				name: "despair",
				children: [
					{ name: "depressed" },
					{ name: "dejected" },
					{ name: "despondent" },
					{ name: "disappointed" },
					{ name: "discouraged" },
					{ name: "disheartened" },
					{ name: "forlorn" },
					{ name: "gloomy" },
					{ name: "heavy hearted" },
					{ name: "hopeless" },
					{ name: "melancholy" },
					{ name: "unhappy" },
					{ name: "wretched" },
				]
			},
			 {
				name: "tired",
				children: [
					{ name: "beat" },
					{ name: "burnt out" },
					{ name: "centered" },
					{ name: "depleted" },
					{ name: "exhausted" },
					{ name: "lethargic" },
					{ name: "listless" },
					{ name: "sleepy" },
					{ name: "fatigue" },
					{ name: "weary" },
					{ name: "worn out" },

				]
			},
		]
	},
}

export {emotionDict}