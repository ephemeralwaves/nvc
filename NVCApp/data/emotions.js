
const emotionDict = {
	hehp: {
		name:"hehp",
		color: "#eaa802",
		children:[
			{name:"ecstatic"},
			{name:"exhilarated"},
			{name:"motivated"},
			{name:"elated"},
			{name:"inspired"},
			{name:"thrilled"},
			{name:"optimistic"},
			{name:"proud"},
			{name:"blissful"},
			{name:"playful"},
		]
	},
	help: {
		name:"help",
		color: "#bf9c44",
		children:[
			{name:"surprised"},
			{name:"upbeat"},
			{name:"hyper"},
			{name:"cheerful"},
			{name:"energized"},
			{name:"lively"},
			{name:"pleasant"},
			{name:"happy"},
			{name:"joyful"},
		]
	},
	hehup: {
		name:"hehup",
		color: "#c90e5a",
		children:[
			{name:"enraged"},
			{name:"panicked"},
			{name:"furious"},
			{name:"fuming"},
			{name:"angry"},
			{name:"frightened"},
			{name:"anxious"},
			{name:"apprehensive"},
			{name:"repulsed"},
			{name:"troubled"},
		]
	},
	helup: {
		name:"helup",
		color: "#a53361",
		children:[
			{name:"shocked"},
			{name:"frustrated"},
			{name:"tense"},
			{name:"stressed"},
			{name:"nervous"},
			{name:"restless"},
			{name:"worried"},
			{name:"annoyed"},
			{name:"irritated"},
			{name:"uneasy"},
			{name:"peeved"},
		]
	},
	lehp: {
		name:"lehp",
		color: "#209754",
		children:[
			{name:"serene"},
			{name:"tranquil"},
			{name:"comfy"},
			{name:"carefree"},
			{name:"balanced"},
			{name:"grateful"},
			{name:"touched"},
			{name:"loved"},
			{name:"fulfilled"},
			{name:"content"},
		]
	},
	lelp: {
		name:"lelp",
		color: "#559671",
		children:[
			{name:"restful"},
			{name:"sleepy"},
			{name:"relieved"},
			{name:"satisfied"},
			{name:"relaxed"},
			{name:"calm"},
			{name:"secure"},
			{name:"humbled"},
			{name:"chill"},
			{name:"at ease"},
		]
	},
	lehup: {
		name:"lehup",
		color: "#203cbc",
		children:[
			{name:"hopeless"},
			{name:"despair"},
			{name:"embarassed"},
			{name:"miserable"},
			{name:"alienated"},
			{name:"mortified"},
			{name:"pessamistic"},
			{name:"disgusted"},
			{name:"dissapointed"},
		]
	},
	lelup: {
		name:"lelup",
		color: "#4f60ad",
		children:[
			{name:"drained"},
			{name:"exhausted"},
			{name:"tired"},
			{name:"bored"},
			{name:"lonely"},
			{name:"sad"},
			{name:"timid"},
			{name:"apathetic"},
		]
	},


}

export {emotionDict}