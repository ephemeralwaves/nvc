import * as React from 'react';
import { StyleSheet,Modal,Text,TouchableOpacity,View,Pressable }from 'react-native';
import {UsersContextProvider,UsersContext} from '../context/UsersContext'
import {useEffect,useContext} from 'react';


export default function RemoveUserModal() {



  const [modalVisible, setModalVisible] =  React.useState(false);
  const [name, setName] = React.useState('');
  const usersContext = useContext(UsersContext)
  const { users, deleteNewUser } = usersContext;
  
   const deleteUser = () => {
    deleteNewUser(name)
  }
  return(
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
 <View style={styles.centeredView}>
          <View style={styles.modalView}>
        <TouchableOpacity 
        onPress={deleteUser}
        style={styles.button}>
        <Text style={styles.buttonText}>Remove Person</Text>
      </TouchableOpacity>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Closeddd</Text>
            </Pressable>
          </View>
        </View>
        </Modal>



    )
  
}

const styles = StyleSheet.create({
  gradientBackground: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: 1500,
  }
  });
