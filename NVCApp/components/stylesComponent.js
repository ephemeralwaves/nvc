import { StyleSheet } from 'react-native';
import {fontPixel,pixelSizeVertical,heightPixel,widthPixel} from './styleNormalization';

export default StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    //zIndex : -5
  },
  viewContainerSteps: {
    flex: 1,
  },
  h2Text:{
    color: "white",
    fontSize: fontPixel(28),
    //paddingBottom:10,
    textAlign: 'center',
    padding:10
  },
  centerHeaderText:{
    color: "#31989e",
    fontSize: fontPixel(40),
    marginTop:65,
    marginBottom:10,
    textAlign: 'center',
  },
  centerHeaderTextNoTopMargin:{
    color: "#31989e",
    fontSize: fontPixel(40),
    marginBottom:10,
    textAlign: 'center',

  },
  centerHeaderTextSmaller:{
    color: "#31989e",
    fontSize: fontPixel(18),
    marginTop:200,
    marginBottom:10,
    textAlign: 'center',
  },
  centerHeaderTextSmallerNoHeader:{
    color: "#31989e",
    fontSize: fontPixel(18),
    marginBottom:10,
    textAlign: 'center',
  },
  whiteCenterHeaderText:{
    color: "#fff",
    fontSize: fontPixel(35),
    marginTop:65,
    marginBottom:10,
    textAlign: 'center',
  },
  centerText:{
    color: "#888",
    fontSize: fontPixel(18),
    paddingLeft:40,    
    paddingRight:40, 
    paddingBottom:15,
    paddingTop:10,
    textAlign: 'center',
    justifyContent: 'center',

  },
  helpScreenHeaderText:{
    color:"white",
    fontSize: fontPixel(25),
    marginBottom:10,
    textAlign: 'center'
  },
  addPersonButtonText:{
    color:"#fff",
    fontWeight: "bold",
    fontSize: fontPixel(30), 
    paddingLeft: 5,
    paddingRight:5,
  },
  dataTableHeader:{
    justifyContent:"center",
    fontSize: fontPixel(13), 
    fontWeight: "bold",
  },
  dataTableTextLeft:{
    flex:1, 
    flexGrow:1, 
    paddingTop:9,
    paddingLeft:15, 
    paddingRight:14
  },
  dataTableTextRight:{
    flex:1, 
    flexGrow:1, 
    paddingTop:9, 
    paddingRight:5
  },
  dataTableTextFont:{
    fontSize: fontPixel(16),
    marginBottom:15
  },
  emotionQuadcenterText:{
    color: "#888",
    fontSize: fontPixel(18),
    paddingLeft:10,    
    paddingRight:10, 
    //paddingBottom:15,
    //paddingTop:10,
    //textAlign: 'center',
    //justifyContent: 'center',

  },

  whiteCenterText:{
    color: "#fff",
    fontSize: fontPixel(18),
    paddingLeft:30,    
    paddingRight:30, 
    paddingBottom:15,
    paddingTop:10,
    textAlign: 'center',
    justifyContent: 'center',
  },
  privacyBodyText:{
    color: "#fff",
    fontSize: fontPixel(20),
    paddingLeft:70,    
    paddingRight:70, 
    paddingBottom:15,
    paddingTop:10,
    textAlign: 'center',
    justifyContent: 'center',
  },
  centerLargeText:{
    color: "#888",
    fontSize: fontPixel(25),
    paddingLeft:40,    
    paddingRight:40, 
    paddingTop:15,
    textAlign: 'center',
    justifyContent: 'center',
  },
  leftText:{
    color: "#888",
    fontSize:fontPixel(18),
    paddingLeft:40,    
    paddingRight:40, 
    paddingBottom:10,

  },


  textInputBox: {
    fontSize: fontPixel(22),
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 10,
    paddingRight:10,
    paddingLeft:10,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:"#eee",
    textAlignVertical:"top"
  },
  logo:{
    resizeMode:'contain', 
    width:200, 
    height:200, 
    margin:10,
  },

  imageTopSmall:{
    resizeMode:'contain', 
    width:100, 
    height:100, 
    margin:10,
    alignSelf:'center',
  },
  walkthroughWhiteCenterHeaderText:{
    color: "#fff",
    fontSize: fontPixel(35),
    marginTop:65,
    marginBottom:10,
    paddingLeft:10,
    paddingRight:10,
    textAlign: 'center',
  },
  walkthroughWhiteCenterText:{
    color: "#fff",
    fontSize: fontPixel(18),
    paddingLeft:30,    
    paddingRight:30, 
    paddingBottom:5,
    paddingTop:5,
    textAlign: 'center',
    justifyContent: 'center',
  },
  walkthroughImageCenter:{
    resizeMode:'contain', 
    width:widthPixel(300), 
    height:heightPixel(400), 
    //margin:5,
    alignSelf:'center',
  },
  walkthroughImageGifCenter:{
    resizeMode:'contain', 
    width:widthPixel(470), 
    height:heightPixel(470), 
    margin:10,
    alignSelf:'center',
  },
  walkthroughVideoCenter:{
    resizeMode:'contain', 
    aspectRatio: 9 / 20,
    width:widthPixel(225), 
    height:heightPixel(500), 
    margin:10,
    alignSelf:'center',
  },

  button: {
    backgroundColor: "white",
    padding: 15,
    margin:10,
    borderRadius: 20,
    paddingHorizontal: 40,
    paddingVertical: 10,
    elevation: 8,
    alignSelf: 'center',
  },
  emotionNextButton: {
    backgroundColor: "white",
    padding: 15,
    margin:10,
    marginBottom:25,
    borderRadius: 20,
    paddingHorizontal: 40,
    paddingVertical: 10,
    elevation: 8,
    alignSelf: 'center',
  },
  redButton: {
    flex:1,
    backgroundColor: "#C90E5A",
    padding: 15,
    //marginTop:pixelSizeVertical(100),
    marginTop:'80%',
    marginBottom:'10%',
    borderRadius: 20,
    paddingHorizontal: 40,
    paddingVertical: 10,
    elevation: 8,
    alignSelf: 'center',
  },
  blueGreenButton: {
    backgroundColor: "#31989e",
    margin:15,
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 10,
    elevation: 8,
    alignSelf: 'center',
  },
  roundButton: {
    //marginTop: 10,
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 200,
    alignSelf: 'center',
    backgroundColor: "white",
    elevation:9,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
  },  
  roundButtonTxt: {
    color: "#31989e",
    fontSize: 14,
    fontWeight: "bold",
    alignSelf: "center",
    textAlign: "center",
    textTransform: "uppercase"
  },
  buttonText: {
      color: "#31989e",
      fontSize: fontPixel(14),
      fontWeight: "bold",
      alignSelf: "center",
      textTransform: "uppercase"
  },
  buttonTextNoColor: {
    fontSize: fontPixel(18),
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
},

  whiteButtonText: {
    color: "#fff",
    fontSize: fontPixel(14),
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
},

  summaryButtonText: {
    color: "#31989e",
    fontSize: fontPixel(14),
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
},
peopleScrollView: {
  flex: 1,
  //flexDirection: "column",

},
peopleBlocks: {
  width: "100%",
  flexDirection: "row",
  marginBottom:7,
  elevation:10,
  backgroundColor: "#31989e",
  padding: 15,
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 5
  },
  shadowOpacity: 0.25,
  shadowRadius: 4,
},
peopleBlocks2: {
  width: "100%",
  flexDirection: "row",
  flexWrap:"wrap",
  marginTop:20,
  marginBottom:2,
  backgroundColor: "#31989e",
  padding: 10,

},

peopleBlocksAddPerson: {
  width: "100%",
  flexDirection: "row",
  marginTop:20,
  marginBottom:2,
  backgroundColor: "#ffffff20",
  padding: 10,

},
peopleElements:{

  color: "#eee",
  fontSize: fontPixel(30),
  paddingLeft: 5,
  paddingRight:5,
},
personSelectText: {
  color: "#eee",
  fontSize: fontPixel(20),
  padding: 14,
},
  interactionContainer:{
    flex:1,

  },
  pastInteractionBlocks: {
    marginLeft: 10,
    marginRight: 10,

  },
  pastInteractionButton: {
    flex:1,
    flexWrap:"wrap",
    flexDirection: "row",
    justifyContent:"space-between",
    marginBottom:15,
    borderRadius: 10,
    //backgroundColor: "rgba(255, 254, 254, 0.55)",
    backgroundColor: "#fff",
    borderColor:"rgba(224, 224, 224, 0.57)",
    borderWidth:1,
    borderStyle: "solid",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.15,
    shadowRadius: 10,
    elevation:9,

  },
  pastInteractionText: {
    color: "#333",
    fontSize:fontPixel(18),
    padding: 14,
    width: '100%',
  },
  pastInteractionData: {
    fontSize: fontPixel(18),
    padding: 14,
  },
  pastInteractionTextResolution: {
    fontSize: fontPixel(18),
    padding: 14,
  },

  lightText: {
    color: "#eee",
    fontSize: fontPixel(18),
    padding: 14,
  },
  darkText: {
    color: "#777",
    fontSize: fontPixel(18),
    padding: 14,

  },
  privacyText: {
    color: "#fff",
    fontSize: fontPixel(24),
    padding: 14,
  },
   warningText: {
    color: "#777",
    fontSize: fontPixel(12),
  },
  energySlider:{
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    justifyContent: 'center',
 
  },
  emotionSlider:{
    flex: 1,
    marginLeft: "20%",
    resizeMode:'contain', 
    width:"60%",
    alignItems: 'stretch',
    justifyContent: 'center'
 
  },
  emotionView: {
    flex: 1,
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },

  emotionQuadBox: {
    width: '100%',
    height: '65%',
    flexWrap: 'wrap',
    flexGrow: 1,
    borderColor:"white",
    borderWidth:1,
    borderStyle: "solid",

  },
  emotionQuads: {
    borderRadius: 10,
    paddingTop:30,
    width: '50%',
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',

  },
  emotionColorWheel:{
    resizeMode:'cover', 
    width: '100%',
    height: '100%',
  },
  emotionQuadText: {
    color: '#fff',
    alignItems: 'center',
    padding: 20,
    fontSize: fontPixel(18),
    textShadowColor: "#000",
    textShadowOffset:{ width:1, height:1},
    textShadowRadius: 5,
  },
  emotionQuadEmoji: {
    padding: 20,
    resizeMode:'contain', 
    width:'20%', 
    height:'20%', 

  },
  emotionText: {
    color: '#ccc',
    alignItems: 'center',
    padding: 20,
    fontSize: fontPixel(26),
    fontWeight: "bold",
    textShadowColor: "#222",
    textShadowOffset:{ width:1, height:1},
    textShadowRadius: 5,
  },
  emotionBlocks: {
    flex:1,
    flexGrow: 1, 
    //backgroundColor:"#31989e",
    marginBottom:2,
    elevation: 9,
  },
  needBlocks: {
    flex:1,
    flexGrow: 1, 
    backgroundColor:"#31989e",
    height:'100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderColor:"white",
    borderWidth:1,
  },

  needImages: {
    flex:1,
    resizeMode:'contain', 
    width:"75%", 
    height:"75%", 
    margin:10,
  },

  needsText: {
    flex:1,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    fontSize: fontPixel(20),
    fontWeight: "bold",
  },
  styleText: {
    color: '#fff',
    alignItems: 'center',
    padding: 20,
    fontSize: fontPixel(20),
    fontWeight: "bold",

  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    //flex:1, 
    //flexGrow:1,
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  hr: {
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    marginLeft:"5%",
    marginRight:"5%"
  }


});