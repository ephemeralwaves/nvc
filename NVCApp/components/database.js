import React from 'react'
import * as SQLite from "expo-sqlite"

const db = SQLite.openDatabase('db.db')



const dropDatabaseTablesAsync = async () => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        'DROP TABLE settings;',
        [],
        (_, result) => { resolve(result) },
        (_, error) => { console.log("error dropping settings table"); reject(error)
        }
      )
      tx.executeSql(
        'DROP TABLE interactions;',
        [],
        (_, result) => { resolve(result) },
        (_, error) => { console.log("error dropping interactions table"); reject(error)
        }
      )
       tx.executeSql(
        'DROP TABLE people;',
        [],
        (_, result) => { resolve(result) },
        (_, error) => { console.log("error dropping people table"); reject(error)
        }
      )
    })
  })
}

const setupDatabaseAsync = async () => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
        tx.executeSql("PRAGMA foreign_keys=ON");
        tx.executeSql(
          `CREATE TABLE IF NOT EXISTS settings (
            id INTEGER PRIMARY KEY NOT NULL, 
            name TEXT,
            value TEXT);`
        );
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY NOT NULL, name TEXT);");
        tx.executeSql(
            `CREATE TABLE IF NOT EXISTS interactions (
              id INTEGER PRIMARY KEY NOT NULL, 
              created DATETIME DEFAULT current_timestamp,
              occurrence_date TEXT,
              interaction TEXT,
              observation TEXT, 
              emotion TEXT,
              need TEXT,
              request TEXT,
              resolved INTEGER CHECK (resolved IN (0, 1)),
              person_id INTEGER NOT NULL, 
              FOREIGN KEY (person_id) REFERENCES people (id) ON DELETE CASCADE);`
        );
      },
      (_, error) => { console.log("db error creating tables"); console.log(error); reject(error) },
      (_, success) => { resolve(success)}
    )
  })
}

const setupPeopleAsync = async () => {
  return new Promise((resolve, _reject) => {
    db.transaction( tx => {
        tx.executeSql( 'INSERT INTO people (id, name) VALUES (?,?)', [1, "Charlie"] );
        tx.executeSql( 'INSERT INTO people (id, name) VALUES (?,?)', [2, "Giselle"] );
      },
      (t, error) => { console.log("db error insertPerson"); console.log(error); resolve() },
      (t, success) => { resolve(success)}
    )
  })
}

const setupInteractionsAsync = async () => {
  return new Promise((resolve, _reject) => {
    db.transaction( tx => {
        tx.executeSql( `INSERT INTO interactions (id,occurrence_date, interaction,observation,emotion,
        need,request, person_id) VALUES (?,?,?,?,?,?,?,?);`,
         [1,"2022-03-19", "Gratitude","observation", "emotion here", "need", "request", 1] );
        tx.executeSql( 'INSERT INTO interactions (id,created,occurrence_date, interaction,observation,resolved, person_id) VALUES (?,?,?,?,?,?,?);',
         [2,"2022-03-17 18:28:03","2022-03-17", "Conflict","Charlie ate the last piece of bread from the kitchen pantry.",0,2] );
        tx.executeSql( 'INSERT INTO interactions (id,created,occurrence_date, interaction,observation,resolved, person_id) VALUES (?,?,?,?,?,?,?);',
         [3,"2022-03-18 18:28:03","2022-03-18", "Gratitude","Charlie helped me with my project.",1,2] );
      },
      (t, error) => { console.log("db error insertInteraction"); console.log(error); resolve() },
      (t, success) => { resolve(success)}
    )
    
  })
}

const setupSettingsAsync = async () => {
  return new Promise((resolve, reject) => {
    console.log("Starting setup of settings...");

    db.transaction( tx => {
      //initialize settings
      console.log("Executing SQL to insert/update settings...");
      //add settings here and update values using the settingsContext
        tx.executeSql( 'INSERT INTO settings (id, name, value) VALUES (?,?,?)', [1, "Walkthrough", "on"] );
    },
        (t, error) => { console.log("db error insertSettings"); console.log(error); resolve() },
        (t, success) => { console.log("db success in the setupSettingAsync Func"); resolve(success)}
      )
    })
  
}


export const database = {
  setupDatabaseAsync,
  setupPeopleAsync,
  setupSettingsAsync,
  dropDatabaseTablesAsync,
  setupInteractionsAsync,
}