import * as React from 'react';
import {
  HeaderButtons,
  HiddenItem,
  OverflowMenu,
  Divider,
} from 'react-navigation-header-buttons';
import MaterialHeaderButton from './MaterialHeaderButton';
import { Ionicons } from '@expo/vector-icons';

export default function TopLeftMenu(navigation, screen, singleInteraction) {
  return (
    <HeaderButtons HeaderButtonComponent={MaterialHeaderButton} >
<Ionicons name="help-circle-outline" size={33}  color={(screen=='Emotion Help') ? "white":"grey"} style={{ marginRight: -15}} onPress={() => navigation.navigate(screen)}  /> 
       
  </HeaderButtons>
  );
}

/*================================================================
///code for when you want multiple menu items
          <OverflowMenu
            style={{ paddingLeft: 0}}
            OverflowIcon={() => (
              (screen=='Emotion Help') ? <Ionicons name="help-circle-outline" size={30} color="white"  /> : <Ionicons name="help-circle-outline" size={33} color="grey" style={{ marginRight: -15}} /> 
            )}
          >

        <HiddenItem title="Home"   onPress={() => {navigation.navigate('MyDrawer', {
                      screen: 'Home',
                      params: { dateParam:singleInteraction.occurrence_date },
                    });
              }} />
            <Divider />
            <HiddenItem title="Help" onPress={() => navigation.navigate(screen)}  />

           
          </OverflowMenu>

*/
