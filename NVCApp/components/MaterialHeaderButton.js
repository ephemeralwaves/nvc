import * as React from 'react';
import {HeaderButton} from 'react-navigation-header-buttons';
import { MaterialIcons } from '@expo/vector-icons';


export default function MateriaHeaderButton(props) {
  return (
    <HeaderButton
    IconComponent={MaterialIcons}
    iconSize={23}
    color="grey"
     pressColor="blue"
    {...props}
  />
  );
}
