import * as React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { StyleSheet }from 'react-native';


export default class BackgroundComponentEnergy extends React.Component {
render() {
  return(
    <LinearGradient
    start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
    locations={[0,1]}
    // Background Linear Gradient
    colors={['rgba(237,184,43,1)', 'transparent']}
    style={styles.gradientBackground}
  />)
  }
}

const styles = StyleSheet.create({
  gradientBackground: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '100%',
  }
  });
