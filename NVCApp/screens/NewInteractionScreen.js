import * as React from 'react';
import { Text,View,TouchableOpacity }from 'react-native';
import BackgroundComponent from '../components/BackgroundComponent';
import styles from '../components/stylesComponent';
import {useEffect,useContext} from 'react';
import {InteractionsContext} from '../context/InteractionsContext';
import TopLeftMenu from '../components/TopLeftMenu';


export default function NewInteractionScreen({ route,navigation }) {
    const personIdParam = route.params.personIdParam;
    const chosenDateParam=route.params.chosenDateParam;
    const interactionsContext = useContext(InteractionsContext);
   const { addNewInteraction,setSingleInteraction ,getInteractionsId,interactionId,singleInteraction} = interactionsContext;
   
   useEffect(() => {
    getInteractionsId()
    setSingleInteraction(interactionId)
  }, [interactionId] )

    //Custom buttons on top right menu
    React.useLayoutEffect(() => {
      navigation.setOptions({
        headerRight: () => (
          TopLeftMenu(navigation,'Type Help',singleInteraction)
          ),
      });
      }, [navigation]);
  return (
    <View style={styles.viewContainer} accessible={false}>
      <BackgroundComponent />
      <TouchableOpacity  
         onPress={() => {
          addNewInteraction(chosenDateParam,"Conflict",0, personIdParam);
          console.log("newinteractionscreen: "+ interactionId)
          navigation.navigate('Observation',{personIdParam: personIdParam});
        }} style={styles.button}>
        <Text style={styles.buttonText}>Conflict</Text>
      </TouchableOpacity>
      <TouchableOpacity  
         onPress={() => {
          addNewInteraction(chosenDateParam,"Gratitude",0, personIdParam);
          navigation.navigate('Observation',{personIdParam: personIdParam});
        }} style={styles.button}>
        <Text style={styles.buttonText}>Gratitude</Text>
      </TouchableOpacity>
    </View>
    
  );
}