import * as React from 'react';
import { ScrollView, Image, Text,View, Linking }from 'react-native';
import BackgroundComponent from '../components/BackgroundComponent';
import styles from '../components/stylesComponent';
import { Entypo } from '@expo/vector-icons';

export default function HistoryScreen() {

  return (
    <View style={styles.viewContainer}>
    <View style={{padding:40}}></View>
        <BackgroundComponent />
        <ScrollView>
        <View style={styles.imageTopSmall}>
        <Entypo name="heart" size={100} color="white" />
        </View>
        <Text style={styles.h2Text}>History</Text>
        <Text style={styles.whiteCenterText}>
       


The app is largely based on our understanding of <Text style={{textDecorationLine: 'underline',}} onPress={() => { Linking.openURL('https://en.wikipedia.org/wiki/Nonviolent_Communication');
            }}>Nonviolent Communication (NVC) </Text> which was created by 
            <Text
    style={{color: '#fff',textDecorationLine: 'underline'}}
    onPress={() => {Linking.openURL('https://www.cnvc.org/about/marshall')}}
  > Dr. Marshall B. Rosenberg and the Center for  Nonviolent Communication. </Text>. It is an approach to communication based on principles of nonviolence. 
            It is not a technique to end disagreements, but rather a method 
            designed to increase empathy and improve the quality of life of those 
            who utilize the method and the people around them. According to Rosenberg, needs are never in 
            opposition — only our strategies for meeting them are.{"\n"}{"\n"}
The app was also influenced by the ideas and concepts such as <Text
    style={{color: '#fff',textDecorationLine: 'underline'}}
    onPress={() => {Linking.openURL('https://en.wikipedia.org/wiki/I-message')}}
  > I-messages</Text> which was coined by Thomas Gordon, the <Text
    style={{color: '#fff',textDecorationLine: 'underline'}}
    onPress={() => {Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2367156/')}}
  >Circumplex Model of Affect</Text> developed by James Russell, and the many people currently and throughout history who have developed and utilized these types of techniques (Buddhist practice, mindfulness, etc.).  {"\n"}{"\n"}
         </Text>

            <View style={{height:350}}></View>  
   </ScrollView>
    </View>
  );
}