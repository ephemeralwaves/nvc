
import * as React from 'react';
import { ScrollView, Text, View,TouchableOpacity }from 'react-native';
import styles from '../components/stylesComponent';
import { DataTable } from 'react-native-paper';
//    {"\t"}It might also be helpful to have the listener reflect back what was expressed by you.

export default function RequestHelpScreen({ navigation }) {
  return (
    
    <ScrollView style={styles.viewContainerSteps}>
   

    <Text style={styles.leftText}>
    {"\n"}
    {"\t"}
    Think about the interaction, your emotion and your need.
    What could be a way to satify that need?</Text>
    <Text style={styles.leftText}>
    {"\t"}Write a request without passing judgement or being demanding (it is ok if they say no to your request) .
    Requests should be positive(use "for" language instead of "against" language), attainable in the short term, and clear. In some cases,
    requests can be made for oneself to meet the unmet need.
    {"\n"}
    </Text>
    <View style={{flex:1}}>

    </View>

    <DataTable style={{flex:1, flexGrow:1}}>
        <DataTable.Header>
          <DataTable.Title style={{justifyContent:"center"}}><Text style={styles.dataTableHeader}>Vague</Text></DataTable.Title>
          <DataTable.Title style={{justifyContent:"center"}}><Text style={styles.dataTableHeader}>Clear</Text></DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
          <View style={styles.dataTableTextLeft}>
          <Text style={styles.dataTableTextFont}>
          Can you work harder on our relationship?
          </Text>
          </View>
          <View style={styles.dataTableTextRight}>
          <Text style={styles.dataTableTextFont}>
           Would you be willing to go to couple's counseling?
          </Text>
          </View>
        </DataTable.Row>

        <DataTable.Row>
        <View style={styles.dataTableTextLeft}>
          <Text style={styles.dataTableTextFont}>
          I do not want you near me.
          </Text>
          </View>
          <View style={styles.dataTableTextRight}>
          <Text style={styles.dataTableTextFont}>
           Would you be willing to give me some personal space for the next hour?
          </Text>
          </View>
        </DataTable.Row>

        <DataTable.Row>
        <View style={styles.dataTableTextLeft}>
          <Text style={styles.dataTableTextFont}>
          Would you stop cursing.
          </Text>
          </View>
          <View style={styles.dataTableTextRight}>
          <Text style={styles.dataTableTextFont}>
           Would you be willing to explore what needs of yours are met by cursing?
          </Text>
          </View>
        </DataTable.Row>

        <DataTable.Row>
        <View style={styles.dataTableTextLeft}>
          <Text style={styles.dataTableTextFont}>
          Will you pay attention to me?
          </Text>
          </View>
          <View style={styles.dataTableTextRight}>
          <Text style={styles.dataTableTextFont}>
           Would you be willing to repeat or summarize what I say after I'm done speaking?
          </Text>
          </View>
        </DataTable.Row>


      </DataTable>
    </ScrollView>
  );
}