
import * as React from 'react';
import { Text, ScrollView,View }from 'react-native';
import styles from '../components/stylesComponent';
import { DataTable } from 'react-native-paper';

export default function ObservationHelpScreen({ navigation }) {

  return (
    
    <ScrollView style={styles.viewContainerSteps}>
    <View styles={{padding:45}}><Text>{"\n"}</Text></View>
    <Text style={styles.leftText}>
      These are a few examples of how to word observations in an non-judmental way.
    </Text>
    <Text style={styles.leftText}> 
    
    </Text>

    <DataTable style={{flex:1, flexGrow:1}}>
        <DataTable.Header>
          <DataTable.Title><Text style={styles.dataTableHeader}>With Judgement</Text></DataTable.Title>
          <DataTable.Title><Text style={styles.dataTableHeader}>Without Judgement</Text></DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
        
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
          <Text style={styles.dataTableTextFont}>
          You keep the house so messy.
          </Text>
          </View>
   
          
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
          <Text style={styles.dataTableTextFont}>
          I recall seeing that your clothes were left on the floor. 
          </Text>
          </View>
          
        </DataTable.Row>

        <DataTable.Row>
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
          <Text style={styles.dataTableTextFont}>
          Amy smells. 
          </Text>
          </View>
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
          <Text style={styles.dataTableTextFont}>
          Amy's scent does not appeal to me. 
          </Text>
          </View>
        </DataTable.Row>

        <DataTable.Row>
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
          <Text style={styles.dataTableTextFont}>
          Your friend is always here.
          </Text>
          </View>

          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
          <Text style={styles.dataTableTextFont}>
          Your friend came over 9 times in the past month.
          </Text>
          </View>
        </DataTable.Row>

        <DataTable.Row>
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
          <Text style={styles.dataTableTextFont}>
          Women are so emotional.
          </Text>
          </View>

          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
          <Text style={styles.dataTableTextFont}>
          Mary started crying when we were talking.
          </Text>
          </View>
        </DataTable.Row>

        <DataTable.Row>
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
          <Text style={styles.dataTableTextFont}>
          If you don't eat well, you will get sick.
          </Text>
          </View>

          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
          <Text style={styles.dataTableTextFont}>
          If you don't eat well, then I worry that your health will suffer.
          </Text>
          </View>
   
        </DataTable.Row>

        <DataTable.Row>

          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
          <Text style={styles.dataTableTextFont}>
          Alex is bad at cooking.
          </Text>
          </View>

          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
          <Text style={styles.dataTableTextFont}>
          Alex's food did not appeal to me.
          </Text>
          </View>

        </DataTable.Row>

          <DataTable.Row>
            <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
            <Text style={styles.dataTableTextFont}>
            You don't take me seriously.
            </Text>
            </View>

            <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
            <Text style={styles.dataTableTextFont}>
            I saw you rolling your eyes when I was talking.
            </Text>
            </View>
          </DataTable.Row>
          <DataTable.Row>
            <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:14}}>
            <Text style={styles.dataTableTextFont}>
            You don't love me.
            </Text>
            </View>

            <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
            <Text style={styles.dataTableTextFont}>
            I have the thought that you don't love me. One reason why is because the last three times I asked to go out on a date you declined.
            </Text>
            </View>
          </DataTable.Row>
        

      </DataTable>
 
    <View style={{paddingBottom:"15%"}}></View>

    </ScrollView>
  );
}