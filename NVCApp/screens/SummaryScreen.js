import * as React from 'react';
import {  ScrollView,Text, View,TouchableOpacity,Alert}from 'react-native';
import styles from '../components/stylesComponent';
import { useHeaderHeight } from '@react-navigation/elements';

import {useContext, useEffect} from 'react';
import {InteractionsContext} from '../context/InteractionsContext';
import dateFormat from "dateformat";
import TopLeftMenu from '../components/TopLeftMenu';

export default function SummaryScreen({ navigation }) {
  const interactionsContext = useContext(InteractionsContext);
  const { interactions, interactionId, getSingleInteraction
  ,setInteractionResolution,singleInteraction, deleteInteraction,getAllRelevantInteractionsByDate } = interactionsContext;
  let currentDate = new Date();
  let formattedDate= dateFormat(currentDate, "yyyy-mm-dd");
  useEffect(() => {
    getSingleInteraction(interactionId);
  }, [] )
 //Custom buttons on top right menu
 React.useLayoutEffect(() => {
  navigation.setOptions({
    headerRight: () => (
      TopLeftMenu(navigation,'Summary Help',singleInteraction)
    ),
  });
}, [navigation]);

function formatRequestText(){
  if (singleInteraction.request){
    return singleInteraction.request.charAt(0).toLowerCase()+singleInteraction.request.slice(1);
  }
  return ''
}
function formatNeedText(){
  if (singleInteraction.need){
    return singleInteraction.need.toLowerCase();
  }
  return ''
}

const alertToDelete = () =>
  Alert.alert(
    //title
    "Warning",
    "This will delete your interaction",
    [
      {
        text: "Delete",
        cancelable: true,
        style: "default",
        onPress: () => {
          deleteInteraction(interactionId);
          navigation.navigate('MyDrawer', {
            screen: 'Home',
            params: { dateParam:formattedDate },
          });
         }
      },
      {
        text: "Go Back",
        cancelable: true,
        style: "cancel",
      },
    ],

  );


const headerHeight = useHeaderHeight();


  return (
 
    <ScrollView style={{
      backgroundColor:"#fff", 
      flex:1, 
      flexGrow:1,

      }}>
      <View>
        <Text style={Object.assign({},styles.centerHeaderTextNoTopMargin)}>Summary</Text>
        <Text style={styles.leftText} selectable={true}> {singleInteraction.observation}  I felt {singleInteraction.emotion} because I need {formatNeedText()}.
        {'\n'}{'\n'} Would you be willing to {formatRequestText()}{'\n'}</Text>
        <Text style={styles.centerLargeText}>Was the request accepted?{'\n'}</Text>
      </View>
      <View style={{
        flex: 1,
        flexDirection: "row",
        justifyContent: 'space-evenly',
        alignItems: 'baseline',
        }}>
          <TouchableOpacity 
                style={Object.assign({width:"25%"},styles.blueGreenButton)}
                onPress={() => {
                setInteractionResolution(1,interactionId);
                navigation.navigate('MyDrawer', {
                  screen: 'Home',
                  params: { dateParam:singleInteraction.occurrence_date },
                });
                }}>
            <Text style={styles.whiteButtonText}>Yes</Text>
          </TouchableOpacity>
          <TouchableOpacity 
                style={Object.assign({width:"25%"},styles.blueGreenButton)}
                onPress={() => {
                setInteractionResolution(0,interactionId);
                //navigation.navigate('MyDrawer',{dateParam:singleInteraction.occurrence_date});
                navigation.navigate('MyDrawer', {
                  screen: 'Home',
                  params: { dateParam:singleInteraction.occurrence_date },
                });
              }}>
            <Text style={styles.whiteButtonText}>No</Text>
          </TouchableOpacity>
      </View>
    

      <View>
      <TouchableOpacity 
                style={Object.assign({width:"25%"},styles.blueGreenButton)}
                onPress={() => {
                setInteractionResolution(0,interactionId);
                //navigation.navigate('MyDrawer',{dateParam:singleInteraction.occurrence_date});
                navigation.navigate('MyDrawer', {
                  screen: 'Home',
                  params: { dateParam:singleInteraction.occurrence_date },
                });
              }}>
            <Text style={styles.whiteButtonText}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity 
                style={styles.redButton}
                onPress={()=> { alertToDelete()}}
                >
            <Text style={styles.whiteButtonText}>Delete</Text>
          </TouchableOpacity>
      </View>
      
      
  </ScrollView>

  );
}

/*
<View style={{marginBottom:35}}></View>
*/