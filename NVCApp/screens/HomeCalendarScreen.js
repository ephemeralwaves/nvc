import * as React from 'react';
import dateFormat from "dateformat";
import {useContext, useEffect} from 'react';
import { ScrollView, Text,View,TouchableOpacity,Pressable }from 'react-native';
import BackgroundComponent from '../components/BackgroundComponent';
import { useFocusEffect,useIsFocused } from "@react-navigation/native"
import {InteractionsContext} from '../context/InteractionsContext';
import styles from '../components/stylesComponent';
import {Calendar} from 'react-native-calendars';

export default function HomeCalendarScreen({ route, navigation }) {
    const dateParam = route.params.dateParam;
    const interactionsContext = useContext(InteractionsContext);
    const { interactions ,setSingleInteraction,setInteractionId,
        getInteractionsId,dates, getAllInteractionDates,
        getAllRelevantInteractionsByDate,interactionId} = interactionsContext;
    let currentDate = new Date();
    let formattedDate= dateFormat(currentDate, "yyyy-mm-dd");
    const [chosenDate, setchosenDate] = React.useState(dateLogic(dateParam));
    const isFocused = useIsFocused();

    function dateLogic(dateParam){
      if (dateParam === null){
        return formattedDate;
      }
      else{
        return dateParam;
      }
    }

    useFocusEffect(
      React.useCallback(() => {
        getAllRelevantInteractionsByDate(chosenDate); //updates interactions
        getAllInteractionDates(); //updates dates
        getAndMarkDates(); //update date dots/markers
      }, [chosenDate])
    );
    // new interaction button is set for selected date
    useEffect(() => {
      newInteractionBtn()
    }, [chosenDate] )

    //makes the sql date into javascript date object
    function dateFromUTC( dateAsString, ymdDelimiter ) {
        var pattern = new RegExp( "(\\d{4})" + ymdDelimiter + "(\\d{2})" + ymdDelimiter + "(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})" );
        var parts = dateAsString.match( pattern );
    
        return new Date( Date.UTC(
          parseInt( parts[1] )
        , parseInt( parts[2], 10 ) - 1
        , parseInt( parts[3], 10 )
        , parseInt( parts[4], 10 )
        , parseInt( parts[5], 10 )
        , parseInt( parts[6], 10 )
        , 0
        ));
      }
    //formats the date
    function prettyDate(date) {
      let newDate = dateFromUTC(date,'-');
      return (dateFormat(newDate, 'yyyy-mm-dd'))
    }
    function showResolution(value){
      if(value==1){
        return "Resolved"
      }
      else {
        return "Unresolved"
      }
    }

    function showSummary(myObservationTxt){
      if (myObservationTxt == null ){
        return '';
      }
      else{
        if(myObservationTxt.length>=55){
          let string = myObservationTxt.substring(0, 60) + " ...";
          return string;
        }
        else{
          return myObservationTxt;
        }
      }
  
    }

    function showName(name){
      if (name == null ){
        return '';
      }
      else{
        if(name.length>=30){
          let string = name.substring(0, 20) + " ...";
          return string;
        }
        else{
          return name;
        }
      }
  
    }

    function goToCorrectScreen(interaction){
      if(interaction.observation == "" ||interaction.observation == null){
        return 'Observation';
      }
      else if(interaction.emotion == null){
        return 'Energy';
      }
      else if(interaction.need == null){
        return 'Need';
      }
      else if(interaction.interaction == "Gratitude"){
        return 'Summary of Gratitude';
      }
      else if(interaction.request == "" || interaction.request == null){
        return 'Write Request';
      }
      else{
        return 'Summary';
      }
    }

    function typeCheck(interaction){
      if(interaction.interaction == "Conflict"){
        return(
          <Text style={Object.assign((interaction.resolved ? {color: "#209754"} :{color: "#C90E5A"}), styles.pastInteractionData)}>{showResolution(interaction.resolved)}</Text>
        )
      }
      else if(interaction.interaction == "Gratitude"){
        return(
        <Text style={Object.assign({color: "grey"}, styles.pastInteractionData)}> {interaction.interaction}</Text>
        )
      }
    }
    function dateDisplay(date){
        return date.slice(5,7)+"/"+date.slice(8,10)
      }
    const showPastInteractions = () => {
      if (interactions !== undefined && interactions.length > 0){
        return(
          <View style={Object.assign({backgroundColor:"#fff"},styles.interactionContainer)}>
          {newInteractionBtn()}
    
          <ScrollView contentContainerStyle={styles.pastInteractionBlocks}  >
  
            {interactions.map((interaction) => (
              <Pressable 
              onPress={() => {
                navigation.navigate(goToCorrectScreen(interaction),{interactionIdParam: interaction.id},setInteractionId(interaction.id),setSingleInteraction(interaction.id));
              }}
              style={styles.pastInteractionButton} 
              key={interaction.id}
              onLongPress={() => {
                navigation.navigate("Summary",{interactionIdParam: interaction.id},setInteractionId(interaction.id));
              }}>

              <Text style={styles.pastInteractionText}>{showSummary(interaction.observation)} </Text>
  
              <Text style={styles.pastInteractionData}>{showName(interaction.name)} </Text>
              {typeCheck(interaction)}
              </Pressable>
            ))}

        </ScrollView>
        </View>
        )
      }
      else{
        return(
          <View style={Object.assign({backgroundColor:"#fff"},styles.interactionContainer)}>
          <BackgroundComponent />
          {roundInteractionBtn()}
          </View>
        )
      }
    }

    //how the markedDate data looks like
    const markedDates={
        '2022-03-20': {marked: true, dotColor: 'red'},
        '2022-03-21': {marked: true, },
    }
    //helper function to set which dates to mark
    const getAndMarkDates =  () => {
        let markedDates = {};
        markedDates[chosenDate]={selected: true, selectedColor: '#31989e'};
        if (dates !== undefined && dates.length > 0){
          //map over the interactions
          dates.map((record) => (
              markedDates[record.occurrence_date]={marked: true, dotColor: '#31989e'}
          ))
          markedDates[chosenDate]={selected: true, selectedColor: '#31989e'};

        }
        return markedDates;
    }

    const getSelectedDayEvents = (date) => {
      getAllRelevantInteractionsByDate(date);
    }
    const handleDatePress = (day) =>{
        setchosenDate(day.dateString)
    }
    function dateDisplay(date){
        return date.slice(5,7)+"/"+date.slice(8,10)
      }
    const newInteractionBtn = () =>  {

        return(
            <View>
            <TouchableOpacity onPress={() => {
                navigation.navigate('Person Involved',{chosenDateParam:chosenDate});
            }} style={styles.blueGreenButton}>
            <Text style={styles.whiteButtonText}>New Interaction</Text>
            </TouchableOpacity>
            </View>
        )
    }

    const roundInteractionBtn = () =>  {

      return(
          <View style={{
            flex:1,
            justifyContent: 'center',

            }}>
          <TouchableOpacity onPress={() => {
              navigation.navigate('Person Involved',{chosenDateParam:chosenDate});
          }} style={styles.roundButton}>
          <Text style={styles.roundButtonTxt}>New Interaction</Text>
          </TouchableOpacity>
          
          </View>
      )
  }

    return (
        <View style={styles.viewContainerSteps}>
           
            <Calendar
              style={{
                        height: 310,
                        selectedDayTextColor: '#eee',
                        arrowColor: '#31989e',
                        fontSize: 33
                    }}
                markedDates={getAndMarkDates()}
                //selected={ dateFormat(currentDate, "yyyy-mm-dd")}
                // Handler which gets executed on day press. Default = undefined
                onDayPress={day => {
                    handleDatePress(day);
                    getSelectedDayEvents(day.dateString);
                }}
                onDayLongPress={day => {
                    handleDatePress(day);
                }}
                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                monthFormat={'MMMM yyyy'}
                firstDay={7}
                onPressArrowLeft={subtractMonth => subtractMonth()}
                onPressArrowRight={addMonth => addMonth()}
                // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
                disableAllTouchEventsForDisabledDays={true}
                // Enable the option to swipe between months. Default = false
                enableSwipeMonths={true}
            />
            <View style={{padding:5,backgroundColor: "#fff",}}></View>
            {showPastInteractions()}
 

        
        </View>
    );
    
}
