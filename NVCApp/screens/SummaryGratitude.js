import * as React from 'react';
import {  ScrollView,Text, View,TouchableOpacity,Alert }from 'react-native';
import styles from '../components/stylesComponent';
import dateFormat from "dateformat";
import {useContext, useEffect} from 'react';
import {InteractionsContext} from '../context/InteractionsContext';
import { useHeaderHeight } from '@react-navigation/elements';
import TopLeftMenu from '../components/TopLeftMenu';

export default function SummaryScreen({ navigation }) {
  const interactionsContext = useContext(InteractionsContext);
  const { interactionId, getSingleInteraction,setInteractionResolution,singleInteraction, deleteInteraction } = interactionsContext;
  let currentDate = new Date();
  let formattedDate= dateFormat(currentDate, "yyyy-mm-dd");
  useEffect(() => {
    getSingleInteraction(interactionId);
  }, [] )
  const headerHeight = useHeaderHeight();
   //Custom buttons on top right menu
 React.useLayoutEffect(() => {
  navigation.setOptions({
    headerRight: () => (
      TopLeftMenu(navigation,'Summary Help',singleInteraction)
    ),
  });
}, [navigation]);

  function formatNeedText(){
    if (singleInteraction.need){
      return singleInteraction.need.toLowerCase();
    }
    return ''
  }

  const alertToDelete = () =>
  Alert.alert(
    //title
    "Warning",
    "This will delete your interaction",
    [
      {
        text: "Delete",
        cancelable: true,
        style: "default",
        onPress: () => {
          deleteInteraction(interactionId);
          navigation.navigate('MyDrawer', {
            screen: 'Home',
            params: { dateParam:formattedDate },
          });
         }
      },
      {
        text: "Go Back",
        cancelable: true,
        style: "cancel",
      },
    ],

  );


  return (
    <ScrollView style={Object.assign({backgroundColor:"#fff"},styles.viewContainerSteps)}>
    <Text style={Object.assign({},styles.centerHeaderTextNoTopMargin)}>Summary</Text>
    <View style={{margin:10}}></View>
    <Text style={styles.leftText} selectable={true} >{singleInteraction.observation}
    {'\n'}{'\n'}I felt {singleInteraction.emotion} because I needed {formatNeedText()}.{'\n'}</Text>

      <TouchableOpacity 
        style={styles.blueGreenButton}
        onPress={() => {
        setInteractionResolution(1,interactionId);
        navigation.navigate('MyDrawer', {
    screen: 'Home',
    params: { dateParam:singleInteraction.occurrence_date },
  });
      }}
        >
    <Text style={styles.whiteButtonText}>Save</Text>
  </TouchableOpacity>

  <View style={{flex:1, justifyContent: 'flex-end',alignItems: 'center',}}>
  <TouchableOpacity 
        style={styles.redButton}
        onPress={()=> { alertToDelete()}}
        >
    <Text style={styles.whiteButtonText}>Delete</Text>
  </TouchableOpacity>
  </View>
  <View style={{marginBottom:35}}></View>
    </ScrollView>
  );
}