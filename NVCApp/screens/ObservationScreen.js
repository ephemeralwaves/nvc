import * as React from 'react';
import {useEffect,useContext} from 'react';
import { ScrollView,Text,TextInput, View,TouchableOpacity,KeyboardAvoidingView }from 'react-native';
import {InteractionsContext} from '../context/InteractionsContext';
import styles from '../components/stylesComponent';
import TopLeftMenu from '../components/TopLeftMenu';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { Ionicons } from '@expo/vector-icons';

export default function ObservationScreen({ navigation }) {
  const [observationInput, setInput] = React.useState('');
  const interactionsContext = useContext(InteractionsContext);
  const { addNewInteractionObservation,interactionId, getInteractionsId,getSingleInteraction,singleInteraction,setInteractionId } = interactionsContext;

  useEffect(() => {
    checkPastInteraction()
  }, [] )

  //Custom buttons on top right menu
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        TopLeftMenu(navigation,'Observation Help',singleInteraction)
      ),
    });
  }, [navigation]);


  function checkPastInteraction() {
    if(singleInteraction){
      if (singleInteraction.observation !=null || singleInteraction.observation != ""){
        getSingleInteraction(interactionId);
        setInput(singleInteraction.observation)
      }
      else{
        setInput("")
      }
    }
  }

  const insertObservation = () => {
    addNewInteractionObservation(observationInput,interactionId);
  }
  const handleTextInput = (observationInput) => {
    //remove line breaks/tabs
    observationInput = observationInput.replace(/[\r\n\t]/gm, '');
    setInput(observationInput);
  }


  return (
    <KeyboardAwareScrollView enableOnAndroid={true} keyboardShouldPersistTaps='handled' extraScrollHeight={100}
    behavior="padding"
    style={{flex:1, backgroundColor: '#fff', }}
  >
    <View style={Object.assign({backgroundColor:"#fff"},styles.viewContainerSteps)}>
    <View></View>
    <Text style={styles.centerHeaderTextNoTopMargin}>Observation</Text>

    <Text style={styles.centerText} selectable={true}>Describe what happened. Try not to use words that pass judgement or make assumptions
    about intent or how others feel. For examples check out the help menu <Ionicons name="help-circle-outline" size={23} color="#888" />.{"\n"}{"\n"}
    Try starting the sentence with:{"\n"}
    “When I see/hear …”{"\n"}
    </Text>
    <ScrollView>
    <KeyboardAvoidingView>
    <TextInput 
        style={styles.textInputBox}
        multiline = {true}
        numberOfLines={4}
        placeholder="Enter your observation"
        onChangeText={handleTextInput}
        value={observationInput}
      />
      </KeyboardAvoidingView>
      
      <TouchableOpacity 
        style={styles.blueGreenButton}
        onPress={() => {
          insertObservation();
          navigation.push('Energy', 
          {interactionIdParam: interactionId});
        }}
        >
        <Text style={styles.whiteButtonText}>Next</Text>
      </TouchableOpacity>
     
      </ScrollView>
      
    </View>
    </KeyboardAwareScrollView>
  );
}
