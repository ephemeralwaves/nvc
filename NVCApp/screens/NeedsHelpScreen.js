
import * as React from 'react';
import { Text, ScrollView,View }from 'react-native';
import styles from '../components/stylesComponent';
import { DataTable } from 'react-native-paper';

export default function NeedsHelpScreen({ navigation }) {

  return (
    
    <ScrollView style={styles.viewContainerSteps}>
    <View style={{paddingBottom:"5%"}}></View>

    <Text style={styles.leftText}>This app attempts to explain emotions due to met or unmet universal needs.
    </Text>


    <DataTable style={{flex:1, flexGrow:1}}>
        <DataTable.Header>
          <DataTable.Title style={{justifyContent:"center"}}><Text style={styles.dataTableHeader}>Need</Text></DataTable.Title>
          <DataTable.Title style={{justifyContent:"center"}}><Text style={styles.dataTableHeader}>Request</Text></DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
          <View style={{flex:1, flexGrow:1, paddingTop:9,paddingLeft:15, paddingRight:14}}>
          <Text style={styles.dataTableTextFont}>
          Universal human quality without reference to place, person or time
          </Text>
          </View>
          <View style={{flex:1, flexGrow:1, paddingTop:9, paddingRight:5}}>
          <Text style={styles.dataTableTextFont}>
          A specific strategy to meet a need that usually involves a particular
          person doing something at a particular time  
          </Text>
          </View>
        </DataTable.Row>
      </DataTable>

    <Text style={styles.leftText}>{"\n"}State the need that is the cause of that feeling. 

    If you're empathizing with someone try and guess the need that caused the feeling, and ask. 
    {"\n"}{"\n"}
    When our needs are met, we have happy, positive feelings; when they are not met,
    we have negative feelings. By tuning into the feeling, you can often find the underlying need.</Text>
 
    <View style={{paddingBottom:"15%"}}></View>

    </ScrollView>
  );
}