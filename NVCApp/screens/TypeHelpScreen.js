
import * as React from 'react';
import { Text, ScrollView,View,TouchableOpacity }from 'react-native';
import styles from '../components/stylesComponent';
import { DataTable } from 'react-native-paper';


//    <Text style={styles.centerHeaderText}>Conflict or Gratitude?</Text>
export default function TypeHelpScreen({ navigation }) {

  return (
    
    <ScrollView style={styles.viewContainerSteps}>
    
    <View style={{paddingBottom:"5%"}}></View>
    <Text style={styles.leftText}>You can choose between either type of interaction. If you think there is a mixture
    of emotions, it might be helpful to start a conflict situation with a gratitude. 
    {"\n"}
   
    </Text>


    <DataTable style={{flex:1, flexGrow:1}}>
        <DataTable.Header>
          <DataTable.Title style={styles.dataTableHeader}><Text style={styles.dataTableHeader}>Conflict</Text></DataTable.Title>
          <DataTable.Title style={styles.dataTableHeader}><Text style={styles.dataTableHeader}>Gratitude</Text></DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
          <View style={styles.dataTableTextLeft}>
          <Text style={styles.dataTableTextFont}>
          Involves all 4 steps of the cycle (observation, emotion, need, request).

          </Text>
          </View>
          <View style={styles.dataTableTextRight}>
          <Text style={styles.dataTableTextFont}>
          Involves the first 3 steps of the cycle (observation, emotion, need).

          </Text>
          </View>
        </DataTable.Row>

        <DataTable.Row>
          <View style={styles.dataTableTextLeft}>
          <Text style={styles.dataTableTextFont}>
          Assumes that the emotion is unpleasant to neutral. 
          </Text>
          <TouchableOpacity 
          onPress={() => {
            navigation.navigate('Emotion Help');
          }}
           >
            <Text style={{textDecorationLine: 'underline',}}>See Emotion Help Screen</Text>
          </TouchableOpacity>

          </View>
          <View style={styles.dataTableTextRight}>
          <Text style={styles.dataTableTextFont}>
          Assumes that the emotion is pleasant to neutral.
          </Text>
          <TouchableOpacity 
          onPress={() => {
            navigation.navigate('Emotion Help');
          }}
           >
            <Text style={{textDecorationLine: 'underline',}}>See Emotion Help Screen</Text>
          </TouchableOpacity>
          </View>
        </DataTable.Row>
        <DataTable.Row>
          <View style={styles.dataTableTextLeft}>

          </View>
          <View style={styles.dataTableTextRight}>
          <Text style={styles.dataTableTextFont}>
          Speaks to what's alive in ourselves (feelings & needs met) stimulated by the other person's actions and
          avoid any judgement of the other person.
          </Text>
          </View>
        </DataTable.Row>
      </DataTable>


    <View style={{paddingBottom:"15%"}}></View>

    </ScrollView>
  );
}