import * as React from 'react';
import { ScrollView, Image, Text,View,TouchableOpacity }from 'react-native';
import {InteractionsContext} from '../context/InteractionsContext';
import styles from '../components/stylesComponent';
import {useContext,useEffect} from 'react';
import {needsDict} from '../data/needs.js';
import TopLeftMenu from '../components/TopLeftMenu';

export default function NeedScreen2({ route,navigation }) {

  const { needTypeParam } = route.params;
//  const [needInput, setInput] = React.useState('');
  const interactionsContext = useContext(InteractionsContext);
  const { addNewInteractionNeed,singleInteraction,getSingleInteraction,interactionId } = interactionsContext;

  useEffect(() => {
    getSingleInteraction(interactionId);
  }, [] )
  //Custom buttons on top right menu
  React.useLayoutEffect(() => {
      navigation.setOptions({
        headerRight: () => (
          TopLeftMenu(navigation,'Needs Help',singleInteraction)
          ),
      });
  }, [navigation]);

  const insertNeed = (needInput) => {
    addNewInteractionNeed(needInput,interactionId);
  }
  function showRelevantText(interaction){
    if(interaction.interaction == "Conflict"){
      return(
        <Text style={styles.leftText}>I felt {interaction.emotion} because I need...</Text>
      )
    }
    else if(interaction.interaction == "Gratitude"){
      return(
        <Text style={styles.leftText}>I felt {interaction.emotion} because I needed...</Text>
      )
    }
  }
  let screenName ="";
  const getCorrectScreen = (interaction) => {
    if(interaction.interaction == "Conflict"){
      screenName = "Write Request"
    }
    else if(interaction.interaction == "Gratitude"){
      screenName = "Summary of Gratitude"
    }
  
  }
  return (
    <ScrollView persistentScrollbar={true} showsVerticalScrollIndicator={true} style={{flex:1, backgroundColor: '#fff', }}>
      <Text style={styles.centerHeaderTextNoTopMargin}>Need</Text>
      <Text style={ styles.leftText}>Observation: {singleInteraction.observation} </Text>  
      {showRelevantText(singleInteraction)}
      <View style={{margin:10}}></View>
      {getCorrectScreen(singleInteraction)}
 
    <ScrollView  style={{flex:1 }}>
     { needsDict[needTypeParam].children.map((need, key)=>(
                <TouchableOpacity
                    key={key} 
                    style={styles.needBlocks}
                    onPress={() => {
                    insertNeed(need.name);
                    navigation.push(screenName);
                    }}
                ><Text style={styles.needsText}>{need.name}</Text>
                </TouchableOpacity>
                ))}
                
      </ScrollView>

      <View>      
      <TouchableOpacity 
        style={styles.button}
        onPress={() => {
        navigation.push(screenName);
      }}
        >
        <Text style={styles.buttonText}>Skip</Text>
      </TouchableOpacity>
      </View>
      <View style={{marginBottom:35}}></View>
    </ScrollView>
  );
}