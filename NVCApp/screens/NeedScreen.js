import * as React from 'react';
import {Text,View,TouchableOpacity,Image,ScrollView }from 'react-native';
import {InteractionsContext} from '../context/InteractionsContext';
import styles from '../components/stylesComponent';
import {useContext,useEffect} from 'react';
import TopLeftMenu from '../components/TopLeftMenu';
import { useHeaderHeight } from '@react-navigation/elements';


export default function NeedScreen({ navigation }) {

  const interactionsContext = useContext(InteractionsContext);
  const { addNewInteractionNeed,singleInteraction,getSingleInteraction ,interactionId } = interactionsContext;
  const headerHeight = useHeaderHeight();

  useEffect(() => {
    getSingleInteraction(interactionId);
  }, [] )
  //Custom buttons on top right menu
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        TopLeftMenu(navigation,'Needs Help',singleInteraction)
        ),
    });
    }, [navigation]);
    
  const needs = [
      {name: "Autonomy", type:"autonomy",source:require("../images/needsIcons/autonomy.png")},
      {name: "Play or Relaxation",type:"play",source:require("../images/needsIcons/play.png")},
      {name: "Connection with others",type:"connection",source:require("../images/needsIcons/connection.png")},
      {name: "Physical Well-Being",type:"physicalwellbeing",source:require("../images/needsIcons/physicalWellBeing.png")},
      {name: "Psychological Well-Being",type:"psychologicalwellbeing",source:require("../images/needsIcons/psychWellBeing.png")},
      ];

  const insertNeed = (needInput) => {
    addNewInteractionNeed(needInput,interactionId);
  }
  function showRelevantText(interaction){
    if(interaction.interaction == "Conflict"){
      return(
        <Text style={styles.leftText}>I felt {interaction.emotion} because I need...</Text>
      )
    }
    else if(interaction.interaction == "Gratitude"){
      return(
        <Text style={styles.leftText}>I felt {interaction.emotion} because I needed...</Text>
      )
    }
  }
// <View style={{margin:20}}></View>
  return (
    <View style={{flex:1,backgroundColor:"#fff" }}>
    
      <Text style={Object.assign({},styles.centerHeaderTextNoTopMargin)}>Need</Text>
      <Text style={styles.leftText}>Observation: {singleInteraction.observation}  </Text>  
      {showRelevantText(singleInteraction)} 
    
   
    
    <View style={{flex:1,backgroundColor:"#31989e" }}>
     { needs.map((need, key)=>(
                <TouchableOpacity
                    key={key} 
                    style={styles.needBlocks}
                    onPress={() => {
                    insertNeed(need.name);
                    navigation.push('Need List',{ 
                      needTypeParam:need.type,
                      needParam: need.name,
                      });}}
                >

                <Text style={styles.needsText}>{need.name}</Text>
                <Image style={styles.needImages} source={need.source}/>
                </TouchableOpacity>
                ))}
      </View>
    </View>
  );
}