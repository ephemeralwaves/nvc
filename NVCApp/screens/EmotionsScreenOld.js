import * as React from 'react';
import { Text,View,TouchableOpacity }from 'react-native';
import styles from '../components/stylesComponent';




export default function EmotionScreen({ route, navigation }) {
  const { observationParam } = route.params;
  return (
    <View style={{  flex: 1, justifyContent: 'space-between',backgroundColor: '#fff',}}>
    <Text style={ styles.darkText}>Observation:  {(observationParam)} </Text>   
    <Text style={ styles.darkText}>I felt: </Text>
      <TouchableOpacity
        style={Object.assign({backgroundColor:"#EDB92B"},styles.emotionBlocks)}
        onPress={() => {navigation.push('Need', {emotionParam: 'happy',observationParam: observationParam});}}
      ><Text style={styles.emotionText}>Happy</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={Object.assign({backgroundColor:"#C90E5A"},styles.emotionBlocks)}
        onPress={() => {navigation.push('Need', {emotionParam: 'angry',observationParam: observationParam});}}
      ><Text style={styles.emotionText}>Angry</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={Object.assign({backgroundColor:"#2647E0"},styles.emotionBlocks)}
        onPress={() => {navigation.push('Need', {emotionParam: 'sad',observationParam: observationParam});}}
        ><Text style={styles.emotionText}>Sad</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={Object.assign({backgroundColor:"#3A0047"},styles.emotionBlocks)}
        onPress={() => {navigation.push('Need', {emotionParam: 'scared',observationParam: observationParam});}}
        ><Text style={styles.emotionText}>Scared</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={Object.assign({backgroundColor:"#FC762B"},styles.emotionBlocks)}
        onPress={() => {navigation.push('Need', {emotionParam: 'surprised',observationParam: observationParam});}}
        ><Text style={styles.emotionText}>Surprised</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={Object.assign({backgroundColor:"#209754"},styles.emotionBlocks)}
        onPress={() => {navigation.push('Need', {emotionParam: 'disgusted',observationParam: observationParam});}}
        ><Text style={styles.emotionText}>Disgusted</Text>
      </TouchableOpacity>
      
    </View>
  );
}

