import * as React from 'react';
import { ScrollView,Linking, ImageBackground, Text,View,TouchableOpacity, Image }from 'react-native';
import styles from '../components/stylesComponent';
import { Ionicons } from '@expo/vector-icons';
import {fontPixel} from '../components/styleNormalization';

export default function EmotionQuadrantScreen( ) {

  const [toggle, setToggle] = React.useState(false);


  var emotions = [
      {name: "angry", source:require("../images/emotionIcons/angryw.png"), description:"High Energy Unpleasant", emotionNames:"Anger\nPanic\nDisgust"},
      {name: "sad", source:require('../images/emotionIcons/sadw.png'),description:"Low Energy Unpleasant",emotionNames:"Sadness\nTiredness"},
      {name: "happy", source:require('../images/emotionIcons/happyw.png'),description:"High Energy Pleasant",emotionNames:"Happy\nSurprise"},
      {name: "calm", source:require('../images/emotionIcons/calmw.png'),description:"Low Energy Pleasant",emotionNames:"Calm"},
      ];


function showEmoji(emotion) {
  return(           
    <View style={{
    flexWrap: 'wrap',
    flexGrow: 1,
    marginBottom:30,
    alignItems: 'center',
    justifyContent: 'center',
    }}>    
      <Image style={styles.emotionQuadEmoji} source={emotion.source}/>
      <Text style={styles.emotionQuadText}>{emotion.description}</Text>
    </View> )
}
function showEmotionNames(emotion){
  return <Text style={styles.emotionQuadText}>{emotion.emotionNames}</Text>
}

  return (
    <View style={Object.assign({    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.25,},styles.viewContainer)}>
      <ScrollView persistentScrollbar={true} showsVerticalScrollIndicator={true} >

      <Text style={Object.assign({margin:10,flex:1,flexDirection: "row",flexWrap:"wrap"},styles.emotionQuadcenterText)}>
      {"\t"}We use the  <Text
    style={{color: '#31989e',textDecorationLine: 'underline'}}
    onPress={() => {Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2367156/')}}
  >circumplex model of affect</Text>. The axis represent physical stimulation (energy) and pleasantness. 
      Use this as a guide to figure out what you are feeling. {"\n"}{"\n"}
      {"\t"}For conflicts we ask you how much energy and unpleasantness you are feeling (left side of the graph). 
      Gratitudes are associated with the right side.{"\n"}
      Press on <Ionicons name="finger-print-outline" size={fontPixel(20)} color="black" /> the image below to see the corresponding emotions.</Text>

      </ScrollView>
      <View  style={{
        backgroundColor:'grey',
        width:'100%',
        height:'5%', 
        justifyContent: 'space-around',  
        flexDirection: "row",
        flexWrap:"wrap",
        alignItems: 'center',
        borderColor:"white",
        borderWidth:1,
        borderStyle: "solid",
        paddingTop:3
        }}>      
      <Text style={{color:"white", fontSize:fontPixel(18)}}>Conflict </Text>
      <Text style={{color:"white", fontSize:fontPixel(18)}}> Gratitude</Text>
      </View>

        <View style={styles.emotionQuadBox}>
        <ImageBackground style={styles.emotionColorWheel} source={require('../images/color_wheel_sq.jpg')}>
        
        <View style={styles.emotionQuadBox}>     
            { emotions.map((emotion, key)=>(
                <TouchableOpacity
                key={key} 
                style={styles.emotionQuads}
                onPress={() => {
                  if (toggle==false){
                   setToggle(true);
                  }
                  else{
                    setToggle(false)
                  }
                    }}
                >
                {toggle ? showEmotionNames(emotion):showEmoji(emotion)}
                
                </TouchableOpacity>
                ))}
                
            </View>
        </ImageBackground>
        </View>
    </View>
  );
}

