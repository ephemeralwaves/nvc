import * as React from 'react';
import { ScrollView,Button,View, Text, Image }from 'react-native';
import styles from '../components/stylesComponent';
import {useContext, useRef} from 'react';
import { useFocusEffect } from "@react-navigation/native"
import {SettingsContext} from '../context/SettingsContext';
import AppIntroSlider from 'react-native-app-intro-slider';
import { useHeaderHeight } from '@react-navigation/elements';
import { Video, AVPlaybackStatus } from 'expo-av';
import { slides } from '../data/slides';

export default function WalkthroughScreen({ navigation}) {
  const slider = useRef();
  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});
  const headerHeight = useHeaderHeight();
  const settingsContext = useContext(SettingsContext);
  const { updateSetting,walkthroughValue } = settingsContext;

  useFocusEffect(
    React.useCallback(() => {
      slider.current.goToSlide(0);
    }, [])
  );

  const renderItem = ({item}) => {
    if (!(item.image===undefined)){
      return (
        <View style={[{flex:1,backgroundColor: '#DD7673'}]}>
          <Text style={item.titleStyle}>{item.title}</Text>
          <Text style={item.textStyle}>{item.toptext}</Text>
          <Image source={item.image.require} style={item.imageStyle} />
          <Text style={item.textStyle}>{item.bottomtext}</Text>
        </View>
      );
  }
    else if (!(item.video===undefined)){
      return (
        <View style={[{flex:1,backgroundColor: '#DD7673'}]}>
          <Text style={item.titleStyle}>{item.title}</Text>
          <Text style={item.textStyle}>{item.toptext}</Text>
          <Video
        ref={video}
        style={item.videoStyle}
        source={item.video.uri}
        useNativeControls
        resizeMode="contain"
        shouldPlay={true}
        isLooping
        onPlaybackStatusUpdate={status => setStatus(() => status)}
      />
          <Text style={item.textStyle}>{item.bottomtext}</Text>
        </View>
      );
    }
  };

  const keyExtractor = (item) => item.key;

  const onDoneSlides = () => {
    console.log(walkthroughValue+ " is the walkthrough value")
     if (walkthroughValue == true) {
       console.log("in the if of done slides")
       updateSetting("Walkthrough","off");
      }
    navigation.navigate('MyDrawer',{dateParam:null});
    //slider.current.goToSlide(0);
  };

    return ( 
    <AppIntroSlider 
    keyExtractor={keyExtractor}
    renderItem={renderItem}
    data={slides} 
    onDone={onDoneSlides} 
    showSkipButton={true} 
    onSkip={onDoneSlides} 
    ref={(ref) => (slider.current = ref)} 
    /> 
    );  
  
}

