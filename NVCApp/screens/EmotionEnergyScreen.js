import * as React from 'react';
import { Text,View,TouchableOpacity, Image }from 'react-native';
import styles from '../components/stylesComponent';
import {Slider} from '@miblanchard/react-native-slider';
import {useContext,useEffect} from 'react';
import {InteractionsContext} from '../context/InteractionsContext';
import BackgroundComponentEnergy from '../components/BackgroundComponentEnergy';
import TopLeftMenu from '../components/TopLeftMenu';

export default function EmotionEnergyScreen({ route, navigation }) {
  const [value, setValue] = React.useState('.5');
  const interactionsContext = useContext(InteractionsContext);
  const { singleInteraction,getSingleInteraction, interactionId } = interactionsContext;

  useEffect(() => {
    getSingleInteraction(interactionId);
  }, [] )

  //Custom buttons on top right menu
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        TopLeftMenu(navigation,'Emotion Help',singleInteraction)
      ),
    });
  }, [navigation]);
  
  function getRightScreen(){
    if(singleInteraction.interaction == "Gratitude"){
      return "Pleasantness";
    }
    else if(singleInteraction.interaction == "Conflict"){
      return "Unpleasantness";
    }
  }
  function getEnergyLevel(value){
    if(value >= .53){
 
      return "he"
    }
    else{

      return "le"
    }
  }

  
  return (

    <View style={styles.viewContainerSteps}>
      <BackgroundComponentEnergy />

      <Text style={styles.whiteCenterHeaderText}>Energy</Text>
      <Text style={ styles.whiteCenterText}>How do you feel?</Text>
      <Image style={styles.imageTopSmall} source={require('../images/lightning.png')}/>

      <View style={styles.energySlider}>
      <Slider
          thumbTouchSize={{width: 40, height: 40}}
          vertical={true}
          value={value}
          onValueChange={value => setValue(value)}
      />
      </View>
      <View style={{paddingTop: 30}}></View>
        <TouchableOpacity
        style={styles.emotionNextButton}
        onPress={() => {
            navigation.push(getRightScreen(), 
              {energyParam: getEnergyLevel(value)});
            }}
        >

        <Text style={styles.buttonText}>Next</Text>
        </TouchableOpacity>
    </View>

  );
}

