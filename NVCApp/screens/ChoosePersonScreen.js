import * as React from 'react';
import {  Text,View,TouchableOpacity,Modal,Alert,KeyboardAvoidingView,TextInput }from 'react-native';

import { ScrollView } from 'react-native-gesture-handler';
import {useContext} from 'react';
import {PeopleContext} from '../context/PeopleContext';
import styles from '../components/stylesComponent.js';
import { AntDesign } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons';
import {fontPixel} from '../components/styleNormalization';


export default function ChoosePersonScreen({navigation,route}) {
  chosenDateParam = route.params.chosenDateParam;
  const [modalVisible, setModalVisible] =  React.useState(false);
  const [name, setName] = React.useState('');
  const [id, setId] = React.useState('');
  const peopleContext = useContext(PeopleContext);
  const { people,addNewPerson  } = peopleContext;

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View accessible= {false} >
        <Ionicons name="person-add-outline"       
        onPress={() => {
            clearState();setModalVisible(true);}}
        size={24} color="#888" />
 </View>
      ),
    });
  }, [navigation]);

//functions for Modal
  const initialState = {
    name: "",
    action: "add",
  };
  const alertNoName = () =>
  Alert.alert(
    "Please Enter a Name",
    "",
    [
      {
        text: "Go Back",
        cancelable: true,
        style: "cancel",
      },
    ],

  );
  function addPerson() {
    if (name != ''){
    addNewPerson(name)
    clearState();
    }
    else{
      return alertNoName();
    }
  }
  const handleTextInput = (name) => {
    setName(name);
}
const clearState = () => {
  setName('');
  setId(null);
};

const addButton = () => {
    return(
      <ScrollView style={{marginTop:15}} keyboardShouldPersistTaps="always">
      <TouchableOpacity 
      onPress= {()=>{addPerson(); setModalVisible(!modalVisible)}}
      style={styles.button}>
      <Text style={styles.buttonText}>Add</Text>
      </TouchableOpacity>
      </ScrollView>
    )
}
function modalView() {
  return(
    <View style={{flex:1}}>
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}>
      <View style={{flex:1, paddingTop:'20%'}}>
      <View style={styles.modalView}>
     <View style={{ flexGrow: 1,alignSelf: 'flex-end', }}>
      <AntDesign styles={{}} name="close" size={34} color="#666"
          onPress={() => setModalVisible(!modalVisible)}  />
    </View>
          <TextInput style={styles.textInputBox}       
            placeholder="Enter Name" 
            onChangeText={handleTextInput}
            value={name}/>
             <ScrollView keyboardShouldPersistTaps="always">
          {addButton()}
          </ScrollView>
        </View>
      </View>
    </Modal>
  </View>

  )
}

  return (
    
    <View style={{backgroundColor:"#31989e", flex:1}} >
       
      <ScrollView style={{flex:1}} >
      {people.map((person) => (
        <TouchableOpacity 
        onPress={() => {
          navigation.navigate('New Interaction', {personIdParam: person.id, chosenDateParam:chosenDateParam});
        }}
        style={styles.peopleBlocks2} 
        key={person.id}><AntDesign style={styles.peopleElements} name="user" size={24} color="black" />
        <Text style={[styles.peopleElements]}>{person.name}</Text>
        </TouchableOpacity>
      ))}
      <KeyboardAvoidingView>
      {modalView()}
      </KeyboardAvoidingView>
          <TouchableOpacity 
            accessible={true}
          onPress={() => {
            clearState();setModalVisible(true);}}
          style={styles.peopleBlocksAddPerson} >
            <AntDesign name="adduser" size={fontPixel(34)} color="#fff" />
            <Text style={styles.addPersonButtonText}>
              Add Person
            </Text>
          </TouchableOpacity>
        <View style={{marginBottom:35}}></View>
      </ScrollView>



    </View>
  );
}
