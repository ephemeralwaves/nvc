import * as React from 'react';
import { ScrollView, Text,TextInput,TouchableOpacity }from 'react-native';
import styles from '../components/stylesComponent';
import {useContext,useEffect} from 'react';
import {InteractionsContext} from '../context/InteractionsContext';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import TopLeftMenu from '../components/TopLeftMenu';
import { useHeaderHeight } from '@react-navigation/elements';

export default function WriteRequestScreen({navigation }) {
  const [requestInput, setInput] = React.useState('');
  const interactionsContext = useContext(InteractionsContext);
  const { singleInteraction,getSingleInteraction , addNewInteractionRequest ,interactionId} = interactionsContext;
  const headerHeight = useHeaderHeight();

  useEffect(() => {
    getSingleInteraction(interactionId);
    setInput(singleInteraction.request)
  }, [] )


  //Custom buttons on top right menu
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        TopLeftMenu(navigation,'Request Help',singleInteraction)
      ),
    });
  }, [navigation]);

 
  const insertRequest = (requestInput) => {
    addNewInteractionRequest(requestInput,interactionId);
  }
  const handleTextInput = (requestInput) => {
    requestInput = requestInput.replace(/[\r\n\t]/gm, '');
    setInput(requestInput);
  }

  function formatNeedText(){
    if (singleInteraction.need){
      return singleInteraction.need.toLowerCase();
    }
    return ''
  }

  return (
    <KeyboardAwareScrollView
    enableOnAndroid={true} keyboardShouldPersistTaps='handled' extraScrollHeight={100}
    behavior="padding"
    style={{flex:1, backgroundColor: '#fff', }}
  >
    <ScrollView style={Object.assign({backgroundColor:"#fff"},styles.viewContainerSteps)}>
       
        <Text style={Object.assign({},styles.centerHeaderTextNoTopMargin)}>Request</Text>
        <Text style={styles.leftText}>Observation: {singleInteraction.observation}</Text>
        <Text style={styles.leftText}>
        I felt {singleInteraction.emotion} because I need {formatNeedText()}.
         </Text>
        <Text style={styles.leftText}>Would you be willing to...</Text>
    <TextInput
        multiline = {true}
        numberOfLines={4}
        onChangeText={handleTextInput}
        value={requestInput}
        style={styles.textInputBox}
        placeholder="Enter your request"
      /> 

    <TouchableOpacity 
        style={styles.blueGreenButton}
        onPress={() => {
        insertRequest(requestInput);
        navigation.push('Summary');}}
    >
      <Text style={styles.whiteButtonText}>Finish</Text>
    </TouchableOpacity>

    </ScrollView>
    </KeyboardAwareScrollView>
  );
}