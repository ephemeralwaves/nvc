

import * as React from 'react';
import {useContext} from 'react';
import {KeyboardAvoidingView,Alert, Modal,ScrollView, TextInput,Pressable, Text,View,TouchableOpacity }from 'react-native';
import {PeopleContext} from '../context/PeopleContext';
import styles from '../components/stylesComponent';
import { AntDesign } from '@expo/vector-icons';
import { Foundation } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';



export default function PersonsScreen({ navigation }) {
  const [modalVisible, setModalVisible] =  React.useState(false);
  const [name, setName] = React.useState('');
  const [id, setId] = React.useState('');
  const peopleContext = useContext(PeopleContext);
  const {people, addNewPerson, deletePerson, updatePerson } = peopleContext;
  const[action, setAction] =React.useState('');


//   React.useLayoutEffect(() => {
//     navigation.setOptions({
//       headerRight: () => (
//         <View accessible= {false} style={{marginRight:10}} >
//         <Ionicons name="person-add-outline"       
//         onPress={() => {
//             clearState();setModalVisible(true);}}
//         size={24} color="#888" />
//  </View>
//       ),
//     });
//   }, [navigation]);

  const initialState = {
    name: "",
    action: "add",
  };
  const alertNoName = () =>
  Alert.alert(
    "Please Enter a Name",
    "",
    [
      {
        text: "Go Back",
        cancelable: true,
        style: "cancel",
      },
    ],

  );
  const alertToDelete = () =>
  Alert.alert(
    //title
    "Warning",
    "Deleting a person will delete all past interactions associated with that person!",
    [
      {
        text: "Delete",
        cancelable: true,
        style: "default",
        onPress: () => deletePerson(id)
      },
      {
        text: "Go Back",
        cancelable: true,
        style: "cancel",
      },
    ],

  );
  
  function addPerson() {
    if (name != ''){
    addNewPerson(name)
    clearState();
    }
    else{
      return alertNoName();
    }
  }

  const handleTextInput = (name) => {
      setName(name);
  }
  const handleUpdateTextInput=(name,id) =>{
    if (name!= ""){
    updatePerson(name,id); 
    setModalVisible(!modalVisible)
    }
    else{
      alertNoName();
    }
  }

  const clearState = () => {
    setName('');
    setId(null);
  };

  const actionButton = () => {
    if (action == "add"){
      return(
        <ScrollView style={{marginTop:15}} keyboardShouldPersistTaps="always">
        <TouchableOpacity 
        onPress= {()=>{addPerson(); setModalVisible(!modalVisible)}}
        style={styles.button}>
        <Text style={styles.buttonText}>Add</Text>
      </TouchableOpacity>
      </ScrollView>)
    }
    else if (action =="delete"){
      return(
        <View>
        <TouchableOpacity 
        onPress={() => {handleUpdateTextInput(name,id)} }
        style={styles.button}>
        <Text style={styles.buttonText}>Update Person</Text>
        <Text style={styles.warningText}>Change text above and press 'Update Person' </Text>
      </TouchableOpacity>
      <TouchableOpacity 
        //onPress={() => {deletePerson(id); setModalVisible(!modalVisible)} }
        onPress={()=> {setModalVisible(!modalVisible); alertToDelete()}}
        style={styles.button}>
        <Text style={Object.assign({color:"red"},styles.buttonTextNoColor)}>Delete</Text>
      </TouchableOpacity>
      </View>
      )
    }
    return null;
  }

  function modalView() {
    return(
    
      <View style={{flex:1}}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}>
        <View style={{flex:1, paddingTop:'20%'}}>

          <View style={styles.modalView}>
       <View style={{ flexGrow: 1,alignSelf: 'flex-end', }}>
        <AntDesign styles={{}} name="close" size={34} color="#666"
            onPress={() => setModalVisible(!modalVisible)}  />
      </View>
            <TextInput style={styles.textInputBox}       
              placeholder="Enter Name" 
              onChangeText={handleTextInput}
              value={name}/>
               <ScrollView keyboardShouldPersistTaps="always">
            {actionButton()}
            </ScrollView>

 
          </View>
        </View>
      </Modal>
    </View>
  
    )
  }

  return (
    <View style={[Object.assign({backgroundColor:"#888",},styles.viewContainerSteps), modalVisible ? {backgroundColor: 'gba(175,175,175,0.75)'} : '']}>
      <ScrollView keyboardShouldPersistTaps="always" style={{flex:1 }}>
      {people.map((person) => (
        <TouchableOpacity 
        onPress={() => {setAction("delete");setName(person.name);setId(person.id);setModalVisible(true)}}
        style={{
          width: "100%",
          flexDirection: "row",
          flexWrap:"wrap",
          paddingTop:20,
          paddingBottom:9,
          backgroundColor: "#888",
          padding: 10,
          justifyContent: 'space-between'
        }} 
        key={person.id}><Text style={[styles.peopleElements, modalVisible ? {color: 'rgba(175,175,175,0.75)'} : '']}>{person.name}</Text>
<Foundation name="pencil" size={30} color="white" />
        </TouchableOpacity>
      ))}
      <KeyboardAvoidingView>
      {modalView()}
      </KeyboardAvoidingView>
      <TouchableOpacity 
            accessible={true}
          onPress={() => {
            clearState();setAction('add');setModalVisible(true);}}
          style={styles.peopleBlocksAddPerson} >
            <AntDesign name="adduser" size={34} color="#fff" />
            <Text style={styles.addPersonButtonText}>
              Add Person
            </Text>
          </TouchableOpacity>
        <View style={{marginBottom:35}}></View>
      </ScrollView>

    </View>
  );
}