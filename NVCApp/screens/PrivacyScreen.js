import * as React from 'react';
import { ScrollView, Text, View, Linking, TouchableOpacity }from 'react-native';
import BackgroundComponent from '../components/BackgroundComponent';
import styles from '../components/stylesComponent';
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Fontisto } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import {fontPixel} from '../components/styleNormalization';

export default function PrivacyScreen() {
  return (
    <View style={styles.viewContainer}>
    <View style={{padding:40}}></View>
        <BackgroundComponent />
        <ScrollView>
        <View style={styles.imageTopSmall}>
        <Ionicons name="eye-off-outline" size={100} color="#fff" />
        </View>
        <Text style={styles.h2Text}>Privacy Policy</Text>

        <View style={{flex:1, flexDirection:"row", alignItems: 'center', justifyContent: 'center'}}>
        <AntDesign name="clouddownload" size={fontPixel(45)} color="white" />
        <Text style={styles.privacyText}>Data Collection</Text>
        </View>
        <Text style={styles.privacyBodyText}>We do not collect any data from end users.
        All data is processed in the app. This app was created with Expo which contains code for 
        collecting the IDFA or “identification for advertising” but is not used.</Text>
        
        <View style={{flex:1, flexDirection:"row", alignItems: 'center', justifyContent: 'center'}}>
        <Fontisto name="world-o" size={fontPixel(45)} color="white" />
        <Text style={styles.privacyText}>Data Storage</Text>
        </View>
        <Text style={styles.privacyBodyText}>Data is stored on the phone and is never transmitted externally.</Text>
        
        <View style={{flex:1, flexDirection:"row", alignItems: 'center', justifyContent: 'center'}}>
        <FontAwesome name="hourglass-o" size={fontPixel(45)} color="white" />
        <Text style={styles.privacyText}>Data Duration</Text>
        </View>
        <Text style={styles.privacyBodyText}>
        When the app is deleted, all data will also be deleted.</Text>

        <Text style={styles.privacyBodyText}>
          The privacy policy can be viewed:
        </Text>
        
        <TouchableOpacity onPress={() => Linking.openURL('https://thinkcolorful.org/?page_id=1165')}>
          <Text style={styles.privacyBodyText}>HERE</Text>  
        </TouchableOpacity>
 
        
        <View style={{height:350}}></View>  
   </ScrollView>
    </View>
  );
}
