import * as React from 'react';
import { Button,ScrollView, Text,View,TouchableOpacity,TouchableWithoutFeedback }from 'react-native';
import {useContext} from 'react';
import BackgroundComponent from '../components/BackgroundComponent';
import {PeopleContextProvider,PeopleContext} from '../context/PeopleContext';
import styles from '../components/stylesComponent.js';
import { AntDesign } from '@expo/vector-icons'; 

export default function HomeScreen({navigation,screenName}) {
  const peopleContext = useContext(PeopleContext);
  const { people  } = peopleContext;


  React.useLayoutEffect(() => {
    navigation.setOptions({
      // in your app, extract the arrow function into a separate component
      // to avoid creating a new one every time testID={tID("Add Person")}
      headerRight: () => (
        <View accessible= {false} >
        <Button
        onPress={() => {
    navigation.navigate('People');
  }}
        title="Add Person"
        color="#31989e"
        
      /></View>
      ),
    });
  }, [navigation]);
  return (
    
    <View style={styles.viewContainerSteps} accessible={false} >
        <BackgroundComponent />
      <ScrollView contentContainerStyle={styles.peopleScrollView} >
      <Text style={styles.styleText}>Select person involved in the interaction:</Text>
      {people.map((person) => (
        <TouchableOpacity 
          accessible={true}
          accessibilityLabel={person.name}
        onPress={() => {
          navigation.navigate('Calendar', {personIdParam: person.id});
        }}
        style={styles.peopleBlocks} 
        key={person.id}><AntDesign style={styles.peopleElements} name="user" size={24} color="black" /><Text style={[styles.peopleElements]}>{person.name}</Text></TouchableOpacity>
      ))}
      </ScrollView>



    </View>
  );
}
