import * as React from 'react';
import { ScrollView, Text,View,TouchableOpacity }from 'react-native';
import {InteractionsContext} from '../context/InteractionsContext';
import styles from '../components/stylesComponent';
import {useContext, useEffect} from 'react';
import {emotionDict} from '../data/emotions.js';
import TopLeftMenu from '../components/TopLeftMenu';

export default function EmotionScreen2({ route,navigation }) {

  const { emotionParam } = route.params;
  const interactionsContext = useContext(InteractionsContext);
  const { addNewInteractionEmotion,singleInteraction,getSingleInteraction,interactionId } = interactionsContext;

  useEffect(() => {
    getSingleInteraction(interactionId);
  }, [] )

  //Custom buttons on top right menu
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        TopLeftMenu(navigation,'Emotion Help',singleInteraction)
      ),
    });
  }, [navigation]);
  
  const insertEmotion = (emotionInput) => {
    addNewInteractionEmotion(emotionInput,interactionId);
  } 

//{Object.assign({backgroundColor:"#EDB92B"},styles.emotionBlocks)}
  const showRelevantEmotionList= () => {
    //let color = emotionDict[emotionParam].color;
      return(
        <ScrollView style={{flex:1}}>
        {emotionDict[emotionParam].children.map((emotion, key)=>(
                   <TouchableOpacity
                       key={key} 
                       style={Object.assign({backgroundColor:color},styles.emotionBlocks)}
                       onPress={() => {
                        insertEmotion(emotion.name);
                        navigation.push('Need', 
                          {emotionParam: emotion.name});
                        }}
                   >
                   <Text style={styles.styleText}>{emotion.name}</Text>
                   </TouchableOpacity>
                   ))}
         </ScrollView>
      )
    
  }
  let color = emotionDict[emotionParam].color;
  return (
    <View style={{flex:1, backgroundColor: color+'cc', }}>
    <Text style={styles.whiteCenterHeaderText}>Emotion</Text>
      <Text style={styles.whiteCenterText}>Observation:  {singleInteraction.observation} </Text>  
      <Text style={styles.whiteCenterText}>I felt ...</Text>
      <ScrollView persistentScrollbar={true} showsVerticalScrollIndicator={true} style={{flex:1, backgroundColor: color+'cc', }}>
      {showRelevantEmotionList()}
      <View style={{marginBottom:35}}></View>
      </ScrollView>

    </View>
  );
}