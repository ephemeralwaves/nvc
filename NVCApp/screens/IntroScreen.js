import * as React from 'react';
import { Text, View, Image,TouchableOpacity} from 'react-native';
import styles from '../components/stylesComponent';
import {useContext, useEffect} from 'react';
import {SettingsContext} from '../context/SettingsContext';
import BackgroundComponent from '../components/BackgroundComponent';

export default function IntroScreen({navigation}) {
  const settingsContext = useContext(SettingsContext);
  const { getSetting,walkthroughValue ,updateSetting} = settingsContext;


  useEffect(() => {
    getSetting('Walkthrough')
  }, [] )


  console.log("this is my value from the walkthrough  " + walkthroughValue)

  function handleNav() {
    if(walkthroughValue){
      return navigation.navigate('WalkthroughScreen');
    }
    else{
      return navigation.navigate('MyDrawer',{dateParam:null});
    }
  }

    return (
      
      <View style={styles.viewContainer}>
  
        <BackgroundComponent />
        <Text style={styles.h2Text}>NVC Guide</Text>
        <Image style={styles.logo} source={require('../images/roundlogo1.png')}/>
          <Text style={styles.h2Text}>Your guide to Nonviolent Communication. </Text>
        <TouchableOpacity 
        onPress={() => {
          handleNav();
          }} 
          style={styles.button}>
          <Text style={styles.buttonText}>Start</Text>
        </TouchableOpacity>
      </View>
    );
  }