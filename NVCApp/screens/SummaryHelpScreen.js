
import * as React from 'react';
import { ScrollView, Text, View,TouchableOpacity }from 'react-native';
import styles from '../components/stylesComponent';
import { Ionicons } from '@expo/vector-icons';
import {fontPixel} from '../components/styleNormalization';


export default function SummaryHelpScreen({ navigation }) {
  return (
    
    <ScrollView style={styles.viewContainerSteps}>
   
    <Text style={styles.leftText}>
    {"\n"}
</Text>
    <Text style={styles.leftText}>
    {"\t"}Express the statements to the person involved or reflect on them if this was done for yourself. 
    For a conflict, it might also be helpful to have the listener reflect back what was expressed by you.
    If they do, be sure to express your gratitude; if they don't, try to empathize with them.
    {"\n"}{"\n"}
    {"\t"}If you'd like to send the statement to the person involved you
     can press <Ionicons name="finger-print-outline" size={fontPixel(20)} color="black" /> down 
     on the text to copy it and paste it where it needed.
    {"\n"}
    </Text>
    <View style={{flex:1}}>

    </View>


      
    </ScrollView>
  );
}