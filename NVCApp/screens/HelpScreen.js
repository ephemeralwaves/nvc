import * as React from 'react';
import { ScrollView,TouchableOpacity, Text,View}from 'react-native';
import BackgroundComponent from '../components/BackgroundComponent';
import {useContext} from 'react';
import {SettingsContext} from '../context/SettingsContext';
import styles from '../components/stylesComponent';
import { Ionicons } from '@expo/vector-icons';
import {fontPixel} from '../components/styleNormalization';


//<BackgroundComponent />
export default function HelpScreen({navigation}) {
    const settingsContext = useContext(SettingsContext);
   const { getSetting,walkthroughValue ,updateSetting} = settingsContext;
      


  return (
    <View styles={styles.viewContainer}>
      <BackgroundComponent />
      <View style={{padding:40}}></View>
      <ScrollView>
        <View style={styles.imageTopSmall}>
        <Ionicons name="help-circle-outline" size={100} color="#fff" />  
        </View>
        <Text style={ styles.helpScreenHeaderText}>
                Walkthrough
        </Text>
        <TouchableOpacity 
                 style={styles.button}
                onPress={() => {
                navigation.navigate('WalkthroughScreen');
              }}>
            <Text style={styles.buttonText}>View Tutorial</Text>
          </TouchableOpacity>


        <Text style={styles.whiteCenterText}>
        During each step, check out the help menu <Ionicons name="help-circle-outline" size={fontPixel(23)} color="white" /> on the top right for instructions and tips.
        This menu will also allow you to navigate back home. 
        </Text>

        <Text style={ styles.helpScreenHeaderText}>
                Auto-Saves
        </Text>
        <Text style={styles.whiteCenterText}>
                              
                All interactions are automatically saved at each step. If you go home or close the app midway through an interaction, 
                then you can resume that interaction by selecting it on the home screen. 
        </Text>

        <Text style={ styles.helpScreenHeaderText}>
                Quick Delete 
        </Text>
        <Text style={styles.whiteCenterText}>
        For incomplete interactions that you do not want to finish, you can long press on it and be taken to the summary screen so it can be deleted.
        </Text>
<View style={{height:350}}></View>  
    </ScrollView>


    </View>
  );
}
