import * as React from 'react';
import { Text,View,TouchableOpacity, Image }from 'react-native';
import styles from '../components/stylesComponent';
import {Slider} from '@miblanchard/react-native-slider';
import {useContext} from 'react';
import {InteractionsContext} from '../context/InteractionsContext';
import BackgroundComponentPleasantness from '../components/BackgroundComponentPleasantness';
import BackgroundComponentPleasantnessY from '../components/BackgroundComponentPleasantnessY';
import TopLeftMenu from '../components/TopLeftMenu';



export default function EmotionPleasantnessScreen({ route, navigation }) {
  const energyParam = route.params.energyParam;
  const [value, setValue] = React.useState('.5');
  const interactionsContext = useContext(InteractionsContext);
  const { singleInteraction } = interactionsContext;

  //Custom buttons on top right menu
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        TopLeftMenu(navigation,'Emotion Help',singleInteraction)
        ),
    });
  }, [navigation]);
  

  function getPleasantnessLevel(value){
  
    if(value >= .53){
      return "hp"
    }
    else{
      return "lp"
    }
  }
  function getBgColor(){
    if(energyParam == "he"){
      return <BackgroundComponentPleasantnessY />
    }
    else{
      return <BackgroundComponentPleasantness />
    }
  }
  
  return (
    <View style={styles.viewContainerSteps}>

    {getBgColor()}
      <Text style={styles.whiteCenterHeaderText}>Pleasantness</Text>
      <Text style={ styles.whiteCenterText}>How do you feel?</Text>

      <Image style= {styles.imageTopSmall} source={require('../images/emotionIcons/calmw.png')}/>

      <View style={styles.emotionSlider}>
      <Slider
          thumbTouchSize={{width: 40, height: 40}}
          vertical={true}
          value={value}
          onValueChange={value => setValue(value)}
      />
      </View>
      <Image style={styles.imageTopSmall} source={require('../images/emotionIcons/neutralw.png')}/>
                <TouchableOpacity
                style={styles.emotionNextButton}
                onPress={() => {
                    navigation.push('Emotion List', 
                      {emotionParam: energyParam+getPleasantnessLevel(value)});
                    }}
                >
                <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
    </View>
  );
}

