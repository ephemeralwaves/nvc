

import * as React from 'react';
import dateFormat from "dateformat";
import {useContext} from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { ScrollView, Text,View,TouchableOpacity,Button }from 'react-native';
import BackgroundComponent from '../components/BackgroundComponent';
import {InteractionsContext} from '../context/InteractionsContext';
import styles from '../components/stylesComponent';
import {
  HeaderButtons,
  HeaderButton,
  HiddenItem,
  OverflowMenu,
  Divider,
} from 'react-navigation-header-buttons';
import { MaterialIcons } from '@expo/vector-icons';

const MaterialHeaderButton = (props) => {
  // the `props` here come from <Item ... />
  // you may access them and pass something else to `HeaderButton` if you like
  return (
    <HeaderButton
      IconComponent={MaterialIcons}
      iconSize={23}
      // you can customize the colors, by default colors from react navigation theme will be used
      color="grey"
       pressColor="blue"
      {...props}
    />
  );
};


export default function InteractionsScreen({ route, navigation }) {

  const personIdParam = route.params.personIdParam;
  const interactionsContext = useContext(InteractionsContext);
  const { interactions ,interactionId, getRelevantInteractions,getSingleInteraction,getInteractionsId } = interactionsContext;
  let currentDate = new Date();
  let formattedDate= dateFormat(currentDate, "yyyy-mm-dd");
  const [chosenDate, setchosenDate] = React.useState(formattedDate);
   //match with the people id to show that associated interaction
   //using this react navigation hook to handle when user presses 'back' button
  useFocusEffect(
    React.useCallback(() => {
      getRelevantInteractions(personIdParam);
    }, [])
  );
  ///for nav bar settings
  //<Item title="search" iconName="search" onPress={() => alert('search')} />
  React.useLayoutEffect(() => {
    navigation.setOptions({
      // in your app, extract the arrow function into a separate component
      // to avoid creating a new one every time
      headerRight: () => (
        <HeaderButtons HeaderButtonComponent={MaterialHeaderButton}>
          
          <OverflowMenu
            style={{ marginHorizontal: 10 }}
            OverflowIcon={() => (
              <MaterialIcons name="more-horiz" size={23} color="grey" />
            )}
          >
            <HiddenItem title="Home"  onPress={() => navigation.navigate('MyDrawer')} />
            <Divider />
            <HiddenItem title="Calendar View" onPress={() => navigation.navigate('Calendar',{personIdParam:personIdParam})}  />
          </OverflowMenu>
        </HeaderButtons>
      ),
    });
  }, [navigation]);
  //for handling UTC date such as creation date
  function prettyDate(date) {
    let newDate = dateFromUTC(date,'-');
    return (dateFormat(newDate, 'm/dd/yy'))
  }
  //for handling occurance date
  function dateDisplay(date){
    return date.slice(5,7)+"/"+date.slice(8,10)
  }
  function showResolution(value){
    if(value==1){
      return "Resolved"
    }
    else {
      return "Unresolved"
    }
  }
  function showSummary(myObservationTxt){
    if (myObservationTxt == null ){
      return '';
    }
    else{
      if(myObservationTxt.length>=55){
        let string = myObservationTxt.substring(0, 60) + " ...";
        return string;
      }
      else{
        return myObservationTxt;
      }
    }

  }

  //makes the sql date into javascript date object
  function dateFromUTC( dateAsString, ymdDelimiter ) {
    var pattern = new RegExp( "(\\d{4})" + ymdDelimiter + "(\\d{2})" + ymdDelimiter + "(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})" );
    var parts = dateAsString.match( pattern );

    return new Date( Date.UTC(
      parseInt( parts[1] )
    , parseInt( parts[2], 10 ) - 1
    , parseInt( parts[3], 10 )
    , parseInt( parts[4], 10 )
    , parseInt( parts[5], 10 )
    , parseInt( parts[6], 10 )
    , 0
    ));
  }
  
  function goToCorrectScreen(interaction){

    if(interaction.observation == "" ||interaction.observation == null){
      return 'Observation';
    }
    else if(interaction.emotion == null){
      return 'Emotion';
    }
    else if(interaction.need == null){
      return 'Need';
    }
    else if(interaction.interaction == "Gratitude"){
      return 'Summary of Gratitude';
    }
    else if(interaction.request == "" || interaction.request == null){
      return 'Write Request';
    }
    else{
      return 'Summary';
    }
  }
  function typeCheck(interaction){
    if(interaction.interaction == "Conflict"){
      return(
        <Text style={Object.assign((interaction.resolved ? {color: "#209754"} :{color: "#C90E5A"}), styles.pastInteractionData)}>{showResolution(interaction.resolved)}</Text>
      )
    }
    else if(interaction.interaction == "Gratitude"){
      return(
      <Text style={Object.assign({color: "grey"}, styles.pastInteractionData)}> {interaction.interaction}</Text>
      )
    }
  }

  const showPastInteractions = () => {
    if (interactions !== undefined && interactions.length > 0){
      return(

        <View style={styles.interactionContainer}>
        <Text style={styles.styleText}>Past Interactions:</Text>
        <ScrollView contentContainerStyle={styles.pastInteractionBlocks}  >

          {interactions.map((interaction) => (
            <TouchableOpacity 
            onPress={() => {
              navigation.navigate(goToCorrectScreen(interaction),{interactionIdParam: interaction.id});
            }}
            style={styles.pastInteractionButton} 
            key={interaction.id}>

            <Text style={styles.pastInteractionText}>{showSummary(interaction.observation)} </Text>

            <Text style={styles.pastInteractionData}>{dateDisplay(interaction.occurrence_date)} </Text>
            {typeCheck(interaction)}
            </TouchableOpacity>
          ))}
      </ScrollView>
      </View>

      )
    }
    return null;
  }

  return (
    <View style={styles.viewContainer}>
      <BackgroundComponent />

      {showPastInteractions()}

      <TouchableOpacity onPress={() => {
          navigation.navigate('New Interaction',{personIdParam: personIdParam,chosenDateParam:chosenDate});
        }} style={styles.button}>
        <Text style={styles.buttonText}>New Interaction</Text>
      </TouchableOpacity>

    </View>
  );
}