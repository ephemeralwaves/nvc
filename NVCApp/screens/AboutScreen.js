import * as React from 'react';
import { ScrollView, Image, Text,View}from 'react-native';
import * as Application from 'expo-application';
import styles from '../components/stylesComponent';


export default function AboutScreen() {
  return (
    <View styles={Object.assign({backgroundColor:"#31989e"},styles.viewContainer)}>
     <View style={{padding:45,backgroundColor:'#31989e'}}></View>
        
      <ScrollView persistentScrollbar={true} showsVerticalScrollIndicator={true} style={{backgroundColor:'#31989e'}}><Image style={styles.imageTopSmall} source={require('../images/roundlogo3.png')}/>

        <Text style={styles.h2Text}>How Can This App Help?</Text>
        <Text style={styles.whiteCenterText}>At its core,  Nonviolent Communication is about communicating honestly and receiving empathetically, 
        a way of communicating that “leads us to give from the heart.” (Rosenberg). It is a way of understanding behaviors as expressions of unmet needs. For conflicts, this app will walk you through the four key parts: </Text>
         <Image style={{    
          resizeMode:'contain', 
          width:250, 
          height:250, 
          margin:10,
          alignSelf:'center',
          }} source={require('../images/ccycle.png')}/>
          <Text style={styles.whiteCenterText}>
        These steps are used to create statements that you can use with the person 
        you are in conflict with or share as a gratitude. It can also be used as a way to self-reflect or listen more empathetically.</Text>
        <Text style={styles.h2Text}>Gratitudes</Text>
        <Text style={styles.whiteCenterText}> 
        Did you know that this app can also help you write meaningful gratitudes and be used as a gratitude journal? {"\n"}{"\n"}
        The method above can be used to express gratefulness in a way that explains what underlying need was met.
        This can lead to more meaningful connections. Gratitudes are different than compliments in that they
        avoid any judgement of the other person.
         </Text>
        <Text style={styles.h2Text}>What This Can Not Do</Text>
        <Text style={styles.whiteCenterText}> 
        This app is a tool and is not intended as a substitute for therapy. It is important to recognize that emotional intimacy
        requires consent. This can be further complicated when the relationship has a large power imbalance (employer/employee)
        and one person is not in a position to assert a boundary unilaterally, in which case there is an obligation to be more cautious. 
        </Text>
        <Text style={styles.whiteCenterText}>{Application.applicationName} v{Application.nativeApplicationVersion}</Text>
      <View style={{height:550}}></View>  

    </ScrollView>


    </View>
  );
}
