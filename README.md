# Nonviolent Communication app

## Development Environment
* Mac Machine
* NodeJS 20.10.0 or higher
    * [NVM](https://github.com/nvm-sh/nvm?tab=readme-ov-file#installing-and-updating) (node version control)
        *  nvm install 20.10.0
        *  nvm use 20.10.0
* IDE such as VS code
* Expo cli=> yarn global add expo-cli
* Install android studio with default settings and then add virtual devices in order to test android devices, or use a physical android device
* Yarn => npm install --global yarn
* Watchman => brew install watchman
* Install Xcode from the Mac App Store in order to run Simulator (for testing) and/or run on physical ios device
    * Install  Xcode Command Line Tools
* sudo gem install cocoapods

* Official React Native [Setup guide](https://reactnative.dev/docs/environment-setup?guide=native&os=macos)  

See https://reactnative.dev/docs/environment-setup for more details.

Once you have the repo downloaded, navigate to the root ( ```NVCApp```) and install the dependencies:
```
cd NVCApp
yarn install
```

## Troubleshooting
Run expo doctor to check that all dependencies have been made.
```
npx expo-doctor
```

Rebuilding
```
# Clear your yarn cache
yarn cache clean

# Remove node_modules and reinstall dependencies
rm -rf node_modules/
yarn install

# Start Expo while clearing the Metro bundler cache
npx expo start -c

```

* Xcode/Simulator is installed but expo says it needs to be installed
```
sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer

```
    * Then restart expo with => npx expo start

## To run the app in the Expo dev environment

Navigate to the root (```NVCApp```) and start up Node:
```
npx expo start
```

## To build the app

Install [eas-cli and sign in](https://docs.expo.dev/build/setup/)

#### For android 
Documentation is here : https://docs.expo.dev/build-reference/apk/

To build the apk (once eas.json is configured as per the directions) run:

```
eas build -p android --profile preview
```
#### For ios 
To generate a .app file for using on an ios simulator:
`eas build -p ios --profile preview` 
### For publishing to the store
Documentation is here: https://docs.expo.dev/build/setup/
Increment the version number and the android build number then,
`eas build --platform all`

## Debugging
- In a terminal run `react-devtools`, open the app in expo and hit ctrl/cmd+d and select the element inspector.